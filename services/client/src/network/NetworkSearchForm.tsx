import React from "react";
import {
    makeStyles,
    Theme,
    createStyles,
    Box,
    TextField,
    Paper,
    Chip,
    FormControl,
    Input,
    InputLabel,
    MenuItem,
    Select,
    Checkbox,
    FormControlLabel,
    Typography,
} from "@material-ui/core";
import { NodeSearchOptions } from "./network.hook";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 250,
            maxWidth: 500,
        },
        chips: {
            display: "flex",
            flexWrap: "wrap",
        },
        chip: {
            margin: 2,
        },
        formRow: {
            // display: "flex",
            "& > *": {
                flexGrow: 1,
                margin: "8px",
            },
        },
        formHeader: {
            padding: "20px 8px",
        },
    })
);

const tagList = [
    "Tech",
    "Data",
    "Startup",
    "Software Dev",
    "Social Good",
    "Design",
    "Women",
    "Product",
    "Networking",
    "AI",
];

export const NetworkSearchForm = ({
    onSearch,
}: {
    onSearch: (s: NodeSearchOptions) => void;
}) => {
    const classes = useStyles();
    const [state, setState] = React.useState<NodeSearchOptions>({
        hubTags: [],
        communityTags: [],
        inclusiveTags: true,
        name: null,
        depth: 0,
    });

    const handleChange = (event: any) => {
        const name = event.target.name as keyof NodeSearchOptions;
        let update;
        if (name === "inclusiveTags") {
            update = { ...state, [name]: event.target.checked };
        } else {
            update = { ...state, [name]: event.target.value };
        }
        setState(update);
        onSearch(update);
    };

    return (
        <Paper>
            <Typography variant="h5" className={classes.formHeader}>
                Filter Communities
            </Typography>
            <form noValidate autoComplete="off">
                <Box display="flex" flexDirection="column">
                    <Box flexDirection="row" className={classes.formRow}>
                        <FormControl className={classes.formControl}>
                            <InputLabel id="tag-label">Hub Tags</InputLabel>
                            <Select
                                labelId="tag-label"
                                id="hubTags"
                                name="hubTags"
                                multiple
                                value={state.hubTags}
                                onChange={handleChange}
                                input={<Input id="select-multiple-chip-hubs" />}
                                renderValue={(selected) => (
                                    <div className={classes.chips}>
                                        {(selected as string[]).map((value) => (
                                            <Chip
                                                key={value}
                                                label={value}
                                                className={classes.chip}
                                            />
                                        ))}
                                    </div>
                                )}
                            >
                                {tagList.map((name) => (
                                    <MenuItem key={name} value={name}>
                                        {name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <InputLabel id="tag-label">
                                Filter Communities
                            </InputLabel>
                            <Select
                                labelId="community-tag-label"
                                id="communityTags"
                                name="communityTags"
                                multiple
                                value={state.communityTags}
                                onChange={handleChange}
                                input={<Input id="select-multiple-chip-comm" />}
                                renderValue={(selected) => (
                                    <div className={classes.chips}>
                                        {(selected as string[]).map((value) => (
                                            <Chip
                                                key={value}
                                                label={value}
                                                className={classes.chip}
                                            />
                                        ))}
                                    </div>
                                )}
                            >
                                {tagList.map((name) => (
                                    <MenuItem key={name} value={name}>
                                        {name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={state.inclusiveTags}
                                    onChange={handleChange}
                                    name="inclusiveTags"
                                    id="inclusiveTags"
                                />
                            }
                            label="Search inclusively"
                        />
                    </Box>
                    <FormControl>
                        <TextField
                            className={classes.formControl}
                            id="name"
                            name="name"
                            label="Name"
                            value={state.name}
                            onChange={handleChange}
                            style={{ width: "40%" }}
                        ></TextField>
                    </FormControl>

                    {/* <Button color="primary" variant="outlined" onClick={submit}>
                        Filter
                    </Button> */}
                </Box>
            </form>
        </Paper>
    );
};
