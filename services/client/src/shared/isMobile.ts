import { useEffect, useState } from "react";

export function useMobileCheck(): boolean {
    const checkMobile = (width: number) => width <= 768;
    const [isMobile, setIsMobile] = useState<boolean>(
        checkMobile(window.innerWidth)
    );
    function handleWindowSizeChange() {
        setIsMobile(checkMobile(window.innerWidth));
    }
    useEffect(() => {
        window.addEventListener("resize", handleWindowSizeChange);
        return () => {
            window.removeEventListener("resize", handleWindowSizeChange);
        };
    }, []);

    return isMobile;
}
