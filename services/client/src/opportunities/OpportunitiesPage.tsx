import { useState, useEffect } from "react";
import { from, Observable, of, Subscription } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
import axios from "axios";
import { environment } from "../environment";
import { wrapSubscribe } from "../shared/utility";
import {
    createStyles,
    makeStyles,
    Theme,
    List,
    Paper,
    Grid,
    Container,
    CircularProgress,
} from "@material-ui/core";
import { LoadingIndicator } from "../LoadingIndicator/LoadingIndicator";
import { CustomAlert } from "../CustomAlert/CustomAlert";
import { ReactComponent as ClipSvg } from "./clip.svg";

interface Opportunity {
    id: string;
    type: string;
    title: string;
    description: string;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            position: "relative",
        },
        container: {
            margin: theme.spacing(1),
            padding: theme.spacing(1),
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1,
        },
        opportunityItem: {
            backgroundColor: "#EAD637",
            height: "100%",
        },
        billboard: {
            backgroundColor: "#EBEBEB",
            border: "2px solid black",
            borderRadius: "10px",
            boxShadow: "2px 2px 5px black;",
            padding: "20px",
        },
        opportunityTop: {
            height: "auto",
            padding: "10px 10px",
            backgroundColor: "rgba(0,0,0,0.1)",
            borderTopLeftRadius: "4px",
            borderTopRightRadius: "4px",
        },
        opportunityTitle: {
            color: "black",
            fontSize: "1.1rem",
        },
        opportunityLower: {
            padding: "5px 10px 5px 10px",
        },
        opportunityDesc: {
            fontSize: "0.9rem",
        },
        clip: {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "90px",
        },
    })
);

export const OpportunitiesPage = () => {
    const classes = useStyles();
    const [state, setState] = useState<Opportunity[]>([]);
    const [error, setError] = useState(false);
    const getOpportunities = async () => {
        const oppUrl = environment.apiUrl + "/opportunities";
        try {
            const response = await axios.get<Opportunity[]>(oppUrl);
            const data = response.data;
            setState(data);
        } catch (err) {
            setError(true);
        }
    };

    useEffect(() => {
        getOpportunities();
    }, []);

    const opportunityList: any = state.map((opp) => {
        return (
            <Grid item xs={12} sm={6} md={4}>
                <Paper className={classes.opportunityItem}>
                    <div className={classes.opportunityTop}>
                        <span className={classes.opportunityTitle}>
                            {opp.title}
                        </span>
                    </div>
                    <div className={classes.opportunityLower}>
                        <span className={classes.opportunityDesc}>
                            {opp.description}
                        </span>
                    </div>
                </Paper>
            </Grid>
        );
    });

    return error ? (
        <CustomAlert />
    ) : (
        <div className={classes.container}>
            <Container
                fixed
                className={classes.billboard}
                style={{ maxWidth: "fit-content" }}
            >
                <div className={classes.clip}>
                    <ClipSvg />
                </div>
                <Grid container spacing={2}>
                    {state[0] ? opportunityList : <LoadingIndicator />}
                </Grid>
            </Container>
        </div>
    );
};
