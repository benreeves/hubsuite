export interface CommunityEvent {
    id: string;
    community?: {
        name: string;
        primaryColor?: string;
    };
    externalId: string;
    externalRecurringId: string;
    name: string;
    location: string;
    startDate: Date;
    endDate: Date;
    description?: string;
    link?: string;
    communityId: string;
}
