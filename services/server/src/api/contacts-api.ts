import { BasicCrudApi } from "./api-base";
import { Request, Response } from "express";
import {
    CommunityContact,
    Contact,
    HubContact,
    SponsorContact,
} from "../entity/Contact";

type ExtractedEntity = {
    result: "success";
    payload: Contact;
};

type BadRequestExtraction = {
    result: "bad request";
    message: string;
};

type Extraction = ExtractedEntity | BadRequestExtraction;

export class ContactsApi extends BasicCrudApi<Contact> {
    configureRoutes() {
        const routes = this.configureDefaultRoutes();
        return routes;
    }

    protected async create(req: Request, res: Response): Promise<void> {
        let payload = this.contactFromBody(req.body);
        switch (payload.result) {
            case "success":
                const entity = await this.repo.create(payload.payload);
                const results = await this.repo.save(entity);
                res.send(results);
                break;
            case "bad request":
                res.status(400);
                res.send({
                    error: `Contact type ${req.body?.type} does not exist`,
                });
                break;
            default:
                break;
        }
    }

    private contactFromBody(body: any): Extraction {
        const type = body.type;
        const parentId = body.parentId as string;
        if (!parentId) {
            return { result: "bad request", message: "No parent id provided" };
        }
        let payload: CommunityContact | HubContact | SponsorContact;
        switch (type) {
            case "community": {
                payload = new CommunityContact();
                payload.communityId = parentId;
                break;
            }
            case "hub": {
                payload = new HubContact();
                payload.hubId = parentId;
                break;
            }
            case "sponsor": {
                payload = new SponsorContact();
                payload.sponsorId = parentId;
                break;
            }
            default: {
                return {
                    result: "bad request",
                    message: `Contact type ${body?.type} does not exist`,
                };
            }
        }
        payload.avatar = body.avatar;
        payload.email = body.email;
        payload.name = body.name;
        payload.phone = body.phone;
        return { result: "success", payload: payload };
    }
}
