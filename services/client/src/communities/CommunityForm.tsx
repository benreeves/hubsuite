import {
    Box,
    createStyles,
    FormControl,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,
    Theme,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { CommunityFormState } from "./CommunityFormState";
import TextField from "@material-ui/core/TextField";

interface CommunityFormProps extends React.ComponentProps<any> {
    community: CommunityFormState;
    canEdit?: boolean;
    onChange: (obj: { valid: boolean; formState: CommunityFormState }) => void;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        communityFormGroup: {
            display: "flex",
            width: "80%",
            "& > *": {
                flexGrow: 1,
                margin: "20px 20px 20px 0",
            },
        },
        communityForm: {},
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        description: {
            width: "100%",
        },
        contactCard: {
            width: "50%",
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    })
);

export const CommunityForm = (props: CommunityFormProps) => {
    const [state, setState] = useState<CommunityFormState>(props.community);

    // TODO - toggle editability for the form. Only admins should be able to edit
    const [isEdit, setEdit] = useState<boolean>(false);
    const classes = useStyles();

    // extract out form field changes and update our internal form state
    const handleInputChange = (
        event: React.ChangeEvent<{ name?: string; value: unknown }>
    ) => {
        const name = event.target.name as keyof CommunityFormState;
        const update = { ...state, [name]: event.target.value };
        setState(update);
        props.onChange({ valid: true, formState: update });
    };

    return (
        <form className={classes.communityForm} noValidate autoComplete="off">
            <Box flexDirection="column">
                <TextField
                    className={classes.description}
                    id="description"
                    name="description"
                    label="Description"
                    multiline
                    rows={4}
                    onChange={handleInputChange}
                    value={state?.description || ""}
                />
                <Box flexDirection="row" className={classes.communityFormGroup}>
                    <TextField
                        id="externalUrl"
                        name="externalUrl"
                        label="URL"
                        value={state?.externalUrl || ""}
                        onChange={handleInputChange}
                    ></TextField>
                    <TextField
                        id="primaryColor"
                        name="primaryColor"
                        label="Branding Color"
                        value={state?.primaryColor || ""}
                        onChange={handleInputChange}
                    ></TextField>
                    <TextField
                        id="logoUrl"
                        name="logoUrl"
                        label="Logo URL"
                        value={state?.logoUrl || ""}
                        onChange={handleInputChange}
                        // style={{ width: "40%" }}
                    ></TextField>
                </Box>
                <h4>Locale</h4>
                <Box flexDirection="row" className={classes.communityFormGroup}>
                    {/* <TextField id="country" label="Country" value={state?.locale?.country || ''}></TextField> */}
                    <FormControl>
                        <InputLabel id="country-label">Country</InputLabel>
                        <Select
                            labelId="country-label"
                            name="country"
                            id="country"
                            value={state?.country || ""}
                            onChange={handleInputChange}
                        >
                            <MenuItem value={"Canada"}>Canada</MenuItem>
                            <MenuItem value={"US"}>US</MenuItem>
                        </Select>
                    </FormControl>
                    <TextField
                        id="province"
                        name="province"
                        label="Province"
                        onChange={handleInputChange}
                        value={state?.province || ""}
                    ></TextField>
                    <TextField
                        id="city"
                        name="city"
                        label="City"
                        value={state?.city || ""}
                        onChange={handleInputChange}
                    ></TextField>
                </Box>
            </Box>
        </form>
    );
};
