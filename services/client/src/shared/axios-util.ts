import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { Redirect } from "react-router-dom";

export interface DataOrMessage<T> {
    data: T | null;
    message: string | null;
}

export async function axiosGet<T = any>(
    url: string,
    config?: AxiosRequestConfig
): Promise<DataOrMessage<T>> {
    try {
        const rtn = await axios.get<T>(url, config);
        return extractAxios(rtn);
    } catch (error) {
        if (error.response) {
            return extractAxios(error.response);
        } else if (error.request) {
            /*
             * The request was made but no response was received, `error.request`
             * is an instance of XMLHttpRequest in the browser
             */
            console.log(error.request);
            return {
                message: "Hmm something went wrong with the request.",
                data: null,
            };
        } else {
            // Something happened in setting up the request and triggered an Error
            console.log("Error", error.message);
            return {
                message: "Hmm something went terribly wrong.",
                data: null,
            };
        }
    }
}

export function extractAxios<T>(resp: AxiosResponse<T>): DataOrMessage<T> {
    if (resp.status == 401) {
        // redirec?
    }
    if (isOk(resp)) {
        return { data: resp.data, message: null };
    } else {
        const message = extractMessage(resp);
        console.log(resp);
        return { data: null, message: message };
    }
}

export function extractMessage(resp: AxiosResponse) {
    if (resp.status == 400) {
        return `Client error: ${resp.data.error?.message || "bad request"}`;
    }
    if (resp.status >= 400 && resp.status < 500) {
        return `Client`;
        return `Client error: ${resp.data.error?.message || "bad request"}`;
    }
    if (resp.status >= 500) {
        return `We're experiencing technical difficulties. ${resp.data.error?.message}`;
    }
    return "";
}

export function isOk(resp: AxiosResponse) {
    return resp.status < 400;
}
