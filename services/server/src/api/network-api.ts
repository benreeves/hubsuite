import "reflect-metadata";
import express, { Router } from "express";
import { Connection, Repository, SelectQueryBuilder } from "typeorm";
import { Request, Response } from "express";

import { Hub } from "../entity/Hub";
import { NetworkDto, NetworkDtoGroup } from "../dto/NetworkDto";
import { ApiBase, wrapRoute } from "./api-base";
import { Community } from "src/entity/Community";
import { Group } from "src/entity/Group";
import { HubTag } from "src/entity/Tag";

interface NetworkSearchParams {
    hubId: string;
    hubTags?: string[];
    communityTags?: string[];
    inclusiveTags?: boolean;
    name?: string | null;
    depth: number;
    includeParents: boolean;
    includeChildren: boolean;
}

export class NetworkApi extends ApiBase {
    repo: Repository<Hub>;

    constructor(
        protected app: express.Application,
        protected connection: Connection
    ) {
        super(app, "network");
        this.repo = this.connection.getRepository<Hub>(Hub);
    }

    public configureRoutes(): Router {
        const router = express.Router();
        router
            .get(
                "/",
                wrapRoute(async (req, res) => await this.search(req, res))
            )
            .get(
                "/:id",
                wrapRoute(async (req, res) => await this.search(req, res))
            );
        return router;
    }

    public async search(req: Request, res: Response) {
        const [params, err] = bindParams(req);
        if (!params) {
            res.status(400).send(err);
            return;
        } else {
            const r = await this.buildNetwork(params);
            res.json(r);
        }
    }

    /**
     * We build out the nodes and links for the community network.
     *
     * This method isn't ideal, but it's enough for a demo for now. We want to recursively
     * build out the network based on degree of separation. Unfortunately, the database
     * structure isn't ideal to accomplish this properly.
     * @param params
     */
    public async buildNetwork(
        params: NetworkSearchParams
    ): Promise<NetworkDto> {
        // get the children/parent lineage for hubs
        const hubHierarchy = await this.getHubHierarchy(params);
        // convert the hierarchy datastructure into a list of links
        const hubRelations = this.makeHubLinks(hubHierarchy, params.depth);

        // return links;
        // create flat set of all ids
        const idset = new Set(
            hubRelations
                .map((x) => [x.child, x.parent])
                .reduce((prev, cur) => prev.concat(cur), [])
        );
        hubHierarchy.map((x) => idset.add(x.hub_id));

        if (idset.size == 0) {
            return {
                nodes: [],
                links: [],
            };
        }

        let builder = this.repo
            .createQueryBuilder("hub")
            .leftJoinAndSelect("hub.communities", "community")
            .leftJoinAndSelect("hub.children", "child_hub")
            .leftJoinAndSelect("hub.tags", "hub_tags")
            .leftJoinAndSelect("community.tags", "community_tags")
            .leftJoinAndSelect("child_hub.tags", "child_hub_tags")
            .where("hub.id IN (:...ids)", { ids: [...idset] });

        const hubs = await builder.getMany();

        // lazy filtering in memory for communities
        if (params.communityTags) {
            for (let i = 0; i < hubs.length; i++) {
                const h = hubs[i];
                h.communities = h.communities.filter((comm) =>
                    comm.tags.some((tag) =>
                        params.communityTags.includes(tag.value)
                    )
                );
            }
            // builder = builder
            //     .andWhere("community_tags.value IN (:...tags)", {
            //         tags: params.communityTags,
            //     });
        }

        // Ids are guids, so we use both communities and hubs
        const addedCommunities = new Set<string>();

        const nodes: NetworkDtoGroup[] = [];
        for (let i = 0; i < hubs.length; i++) {
            let h = hubs[i];
            if (!addedCommunities.has(h.id)) {
                nodes.push(toDto(h));
                addedCommunities.add(h.id);
            }
            // Add child communities
            for (let j = 0; j < h.communities.length; j++) {
                let c = h.communities[j];
                if (!addedCommunities.has(c.id)) {
                    nodes.push(toDto(c));
                    addedCommunities.add(c.id);
                }
                hubRelations.push({
                    parent: h.id,
                    child: c.id,
                    childType: "community",
                });
            }

            // Add child hubs. There may or may not be duplicate hubs in here.
            // We have to do this to pull in brother relations to the original query.
            // For instance, if two hubs are both children of a parent, the brother
            // hub isn't currently joined anywhere else
            for (let j = 0; j < h.children.length; j++) {
                let hubChild = h.children[j];
                if (!addedCommunities.has(hubChild.id)) {
                    nodes.push(toDto(hubChild));
                    addedCommunities.add(hubChild.id);
                }
                hubRelations.push({
                    parent: h.id,
                    child: hubChild.id,
                    childType: "hub",
                });
            }
        }

        return {
            nodes: nodes,
            links: hubRelations,
        };
    }

    /**
     * Creates the hierarchy of parents and children hubs. The returned data strcuture
     * looks like {hubId: _, child0_id: _, child1_id: _, parent0_id: _, parent1_id: _}
     * @param params
     */
    public async getHubHierarchy(params: NetworkSearchParams): Promise<any[]> {
        let builder = this.repo
            .createQueryBuilder("hub")
            .leftJoin("hub.tags", "hub_tags")
            .select(["hub.id"])
            .where("1 = 1"); // just here so I don't need conditional andWeheres

        // Join the hierarchy of parents and children (direct ancestors only).
        // This method doesn't load sister/aunt relations, only direct lineages
        // Without having a graph db, I imagine we'd have to perform multiple DB
        // queries in order to build the recursive relationship, but for now
        // we can just deal with direct lineages (parent - grand parent - great grandparent)
        for (let i = 0; i < params.depth; i++) {
            const select = [];
            if (params.includeChildren) {
                // join children to depth level
                const leftName = i == 0 ? "hub" : `child${i - 1}`;
                builder = builder.leftJoin(`${leftName}.children`, `child${i}`);
                select.push(`child${i}.id`);
            }
            if (params.includeParents) {
                // join parents to depth level
                const leftName = i == 0 ? "hub" : `parent${i - 1}`;
                builder = builder.leftJoin(`${leftName}.parents`, `parent${i}`);
                select.push(`parent${i}.id`);
            }
            builder = builder.addSelect(select);
        }

        // if hub id is populated, nothing else should be populated. Could error,
        // but no reason to right now
        if (params.hubId) {
            builder = builder.andWhere("hub.id = :id", { id: params.hubId });
        }
        if (params.name) {
            builder = builder.andWhere("LOWER(hub.name) LIKE :name", {
                name: `%${params.name.toLowerCase()}%`,
            });
        }
        if (params.hubTags) {
            builder = builder.andWhere("hub_tags.value IN (:...tags)", {
                tags: params.hubTags,
            });
        }

        const ents = await builder.getRawMany();
        return ents;
    }

    private makeHubLinks(
        ents: any[],
        depth: number
    ): { parent: string; child: string; childType: "hub" | "community" }[] {
        const links: {
            parent: string;
            child: string;
            childType: "hub" | "community";
        }[] = [];

        // Create a set of ids where the key is parent_id | child_id. We use a set
        // because we can get duplicates back from the DB with the way our query is
        // lazily structured
        const linkSet = new Set<string>();

        // iterate trough the entities building out the parent/child relationships
        for (let i = 0; i < ents.length; i++) {
            // Get the root entity relations
            const ent = ents[i];
            if (ent[`child0_id`]) {
                linkSet.add(ent["hub_id"] + "|" + ent["child0_id"]);
            }
            if (ent[`parent0_id`]) {
                linkSet.add(ent["parent0_id"] + "|" + ent["hub_id"]);
            }

            // Get grandparent/grandchildren relationships
            for (let j = 1; i < depth; i++) {
                let cj = ent[`child${j}_id`];
                let cj2 = ent[`child${j - 1}_id`];
                if (cj && cj2) {
                    linkSet.add(cj + "|" + cj2);
                }

                let pj = ent[`parent${j}_id`];
                let pj2 = ent[`parent${j - 1}_id`];
                if (pj && pj2) {
                    linkSet.add(pj2 + "|" + pj);
                }
            }
        }

        linkSet.forEach((x) => {
            const split = x.split("|");
            links.push({ parent: split[0], child: split[1], childType: "hub" });
        });
        return links;
    }
}

function toDto(group: Community | Hub): NetworkDtoGroup {
    return {
        id: group.id,
        name: group.name,
        externalUrl: group.externalUrl,
        description: group.description,
        logoUrl: group.logoUrl,
        primaryColor: group.primaryColor,
        locale: null,
        tags: ((group.tags || []) as any[]).map((x) => x.value),
    };
}

function bindParams(req: Request): [NetworkSearchParams, string | null] {
    try {
        const search: NetworkSearchParams = {
            hubId: req.params.id,
            name: req.query.name as string,
            depth: parseInt(req.query.depth as string),
            includeChildren:
                ((req.query.includeChildren as string) || "true") == "true",
            includeParents:
                ((req.query.includeParents as string) || "true") == "true",
            inclusiveTags: (req.query.inclusiveTags as string) == "true",
        };
        if (req.query.hubTags) {
            const ht = (req.query.hubTags as string)?.split(",");
            search.hubTags = ht;
        }
        if (req.query.communityTags) {
            const ct = (req.query.communityTags as string)?.split(",");
            search.communityTags = ct;
        }
        return [search, null];
    } catch (err: any) {
        return [null, `Could not parse request: ${err.message}`];
    }
}
