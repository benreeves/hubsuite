export interface Hub {
    id: string;
    name: string;
    logoUrl?: string;
    strapline?: string;
    description?: string;
    googleCalendarId?: string;
}
