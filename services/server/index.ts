import { Server } from "./src/server";
import { GCal, GCalFactory, ServiceAccountCreds } from "./src/services/gcal";
import { CalendarSyncService } from "./src/services/calendar-sync";
import express from "express";
import dotenv from "dotenv";
import { seed, resetDb } from "./src/model/seed";
import { createConnection } from "typeorm";
import { Hub } from "./src/entity/Hub";
import { logger } from "./src/logger";

dotenv.config();
logger.info("Starting app");

createConnection()
    .then(
        async (connection) => {
            // Reset and seed database?
            if (process.env["HUBSUITE_SEED"] == "true") {
                logger.info("Seeding");
                try {
                    await resetDb(connection);
                    await seed(connection);
                } catch (err) {
                    logger.error(err);
                }
            } else {
                console.log("Skipping seed");
            }

            // Sync YYC Data community calendars into database
            if (process.env["CALENDAR_SEED"] != "false") {
                try {
                    const factory = new GCalFactory();
                    const sync = new CalendarSyncService(factory, connection);
                    const hub = await connection
                        .getRepository(Hub)
                        .find({ name: "YYC Data Community" });
                    const id = hub[0].id;
                    await sync.syncHub(id);
                    await sync.syncToSharedCalendar(id);
                } catch (e) {
                    logger.error(
                        `Failed syncing calendar. | ${e.message}\n stack trace - ${e.stack}`
                    );
                }
            }

            // start app
            const app = express();
            let port;
            if (process.env.PORT) {
                try {
                    port = parseInt(process.env.PORT);
                } catch {
                    logger.info(`Bad port: ${process.env.PORT}`);
                    port = 8080;
                }
            } else {
                port = 8080;
            }
            const server = new Server(app, connection);
            try {
                logger.info(`Starting on port ${port}`);
                server.start(port);
            } catch (e) {
                logger.error(e);
            }
        },
        (err) => logger.error(err)
    )
    .catch((err) => {
        logger.error(err);
    });
