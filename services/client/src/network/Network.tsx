import {
    Box,
    createStyles,
    makeStyles,
    Theme,
    Typography,
} from "@material-ui/core";
import React, { useRef, useState, useEffect, useCallback } from "react";
import { CommunityCard } from "../communities/CommunityCard";
import { NetworkD3Simulation, NetworkState } from "./network-d3";
import { getSubgraph, NodeSearchOptions } from "./network.hook";
import { NetworkSearchForm } from "./NetworkSearchForm";
import { CommunityNetworkDto } from "./types";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        networkRoot: {
            display: "flex",
            paddingTop: "2em",
            "& #network": {
                height: "600px",
            },
        },
        detailContainer: {
            width: "50%",
        },
        comunityCardContainer: {
            marginTop: "1em",
        },
        networkContainer: {
            width: "50%",
            maxHeight: "600px",
        },
    })
);

const options = {
    width: 500,
    height: 500,
};

// The network provides a singleton linkage between the d3 network and
// the react component. Everyone wants to get with the DOM. She's a hot
// commodity, but the simulation class is her dad ensuring she only
// sees one boy at a time
const network = new NetworkD3Simulation(options);

function useHookWithRefCallback(): [(node: any) => void] {
    const ref = useRef(null);
    const setRef = useCallback((node) => {
        // cleanup from last render
        if (ref.current) {
            // cleanup
        }
        if (node) {
            network.updateRef(node, false);
        }

        // if (node && !sim) {
        //     const s= new NetworkD3Simulation2(node, options);
        //     setSim(s);
        // }

        // Save a reference to the node
        ref.current = node;
    }, []);

    return [setRef];
}

export function Network() {
    const classes = useStyles();
    const [nodes, setNodes] = useState<CommunityNetworkDto | null>(null);

    useEffect(() => {
        getSubgraph({ depth: 0 })
            .then((n) => {
                setNodes(n);
            })
            .catch(console.log);
    }, []);

    const onSearch = (search: NodeSearchOptions) => {
        getSubgraph(search)
            .then((n) => setNodes(n))
            .catch(console.log);
    };
    const [state, setState] = useState<NetworkState>({
        locked: false,
        community: null,
        show: false,
    });
    // TODO: please sweet baby vishnu let there be a better way to do this
    network.onNodeSelectionChange = setState;

    const [ref] = useHookWithRefCallback();

    useEffect(() => {
        if (network && nodes) {
            network.updateData(nodes);
            return () => network.kill();
        } else {
            return () => {};
        }
    }, [nodes]);

    return (
        <div>
            <Box>
                <Typography variant="h3">Network Explorer</Typography>
                <Typography variant="subtitle1">
                    We are all networks. Each node represents a community and
                    the links are connections between communities. Hover over
                    nodes to view details. Click a node to lock the current
                    view. Use the tools on the right to search and filter the
                    network.
                </Typography>
            </Box>

            <div className={classes.networkRoot}>
                <div className={classes.networkContainer}>
                    <div id="network" ref={ref}></div>
                </div>
                <div className={classes.detailContainer}>
                    <NetworkSearchForm onSearch={onSearch} />
                    {state.show ? (
                        <div className={classes.comunityCardContainer}>
                            <CommunityCard
                                community={state.community as any}
                                suppressLink={true}
                            />
                        </div>
                    ) : null}
                </div>
            </div>
        </div>
    );
}
