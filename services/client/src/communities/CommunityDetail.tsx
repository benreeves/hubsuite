import {
    Button,
    Container,
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { communitiesService, toFormState } from "./communities-service";
import { CommunityFormState } from "./CommunityFormState";
import { Contact } from "../types/Contact";
import { CommunityForm } from "./CommunityForm";
import { Hub } from "../types/Hub";
import CommunityHubList from "./CommunityHubList";
import { LoadingIndicator } from "../LoadingIndicator/LoadingIndicator";
import { SnackyBoi } from "../SnackyBoi/SnackyBoi";
import { ContactList } from "../contact/ContactList";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        hubList: {
            width: "50%",
        },
    })
);

export const CommunityDetail = () => {
    const [formState, setFormState] = useState<CommunityFormState | null>(null);
    const [hubs, setHubs] = useState<Hub[]>([]);
    const [contacts, setContacts] = useState<Contact[]>([]);
    const [snackState, setSnackState] = React.useState({
        open: false,
        message: "",
    });
    const classes = useStyles();
    const { id }: { id: string } = useParams();

    // Initialize by getting the current community from route params
    // and fetch the commnities associated hubs
    useEffect(() => {
        // Get the current community
        const communities$ = communitiesService.getCommunity(id);
        const sub = communities$.subscribe((community) => {
            setContacts(community.contacts || []);
            const form = toFormState(community);
            setFormState(form);
        });
        // get the hubs this community is associated with
        const hubs = communitiesService.getHubsForCommunity(id);
        hubs.then((d) => {
            if (d.data) {
                setHubs(d.data);
            } else {
                // error pop up
                setHubs([]);
            }
        });

        return () => sub.unsubscribe();
    }, [id]);

    const handleFormChange = (obj: {
        valid: boolean;
        formState: CommunityFormState;
    }) => {
        setFormState(obj.formState);
    };
    const submitForm = () => {
        if (formState) {
            communitiesService
                .saveForm(formState)
                .then((res) => {
                    if (res.ok) {
                        setSnackState({ open: true, message: "Saved!" });
                    } else {
                        setSnackState({
                            open: true,
                            message: res.err || "Something went wrong",
                        });
                    }
                })
                .catch((err) => console.log(err));
        }
    };
    const onContactEmit = (x: Contact[]) => setContacts(x);
    const handleClose = (
        event: React.SyntheticEvent | React.MouseEvent,
        reason?: string
    ) => {
        if (reason === "clickaway") return;
        setSnackState({ ...snackState, open: false });
    };

    return (
        <Container>
            {!!formState ? (
                <div>
                    <h2>{formState.name}</h2>
                    <CommunityForm
                        community={formState}
                        canEdit={true}
                        onChange={handleFormChange}
                    />
                    <Button
                        color="primary"
                        variant="contained"
                        onClick={submitForm}
                    >
                        Save
                    </Button>
                    <ContactList
                        contacts={contacts}
                        onEmit={onContactEmit}
                        parentId={id}
                    />
                    <h4>Hubs we're part of:</h4>
                    <div className={classes.hubList}>
                        {hubs ? <CommunityHubList hubs={hubs} /> : ""}
                    </div>
                </div>
            ) : (
                <LoadingIndicator />
            )}
            <SnackyBoi
                open={snackState.open}
                handleClose={handleClose}
                message={snackState.message}
            />
        </Container>
    );
};
