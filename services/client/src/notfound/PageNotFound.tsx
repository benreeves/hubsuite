import React from "react";
import { createStyles, makeStyles, Theme, Button } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        errorNo: {
            fontSize: "120px",
            fontWeight: "lighter",
            textAlign: "center",
            height: "0.50em",
        },
        errorMessage: {
            fontSize: "24px",
            textAlign: "center",
        },
    })
);

export const PageNotFound = () => {
    const classes = useStyles();
    return (
        <div>
            <p className={classes.errorNo}>404</p>
            <p className={classes.errorMessage}>
                The page you are looking for <br />
                can't be found.
            </p>
            <p style={{ textAlign: "center" }}>
                <Button variant="outlined" color="default" href="/app/home">
                    Home
                </Button>
            </p>
        </div>
    );
};
