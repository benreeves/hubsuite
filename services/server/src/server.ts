import { Express, Request, Response } from "express";
import express from "express";
import expressWinston from "express-winston";
import winston from "winston";
import path from "path";
import "reflect-metadata";
import { Connection } from "typeorm";
import { createApiComponents } from "./api/routes";

export class Server {
    constructor(private app: Express, private connection: Connection) {}

    public start(port: number): void {
        this.configureMiddleware();
        this.app.listen(port, () =>
            console.log(`Server listening on port ${port}!`)
        );
    }

    protected configureMiddleware() {
        this.app.use(express.static(path.resolve("./") + "/build/client"));
        this.app.use(express.json());
        this.app.use(
            expressWinston.logger({
                transports: [new winston.transports.Console()],
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.json()
                ),
                meta: true,
                msg: "HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
                expressFormat: true,
                colorize: false,
                ignoreRoute: function (req, res) {
                    return false;
                },
            })
        );
        this.configureRoutes();
        // express-winston errorLogger makes sense AFTER the router.
        this.app.use(
            expressWinston.errorLogger({
                transports: [new winston.transports.Console()],
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.json()
                ),
            })
        );

        this.app.use(function (err, req, res, next) {
            // If err has no specified error code, set error code to 'Internal Server Error (500)'
            if (!err.statusCode) {
                err.statusCode = 500;
            }

            res.status(err.statusCode).json({
                status: false,
                error: err,
            });
        });
        // this.app.use((err, req, res, next) => {
        //     if (res.headersSent) {
        //         return next(err);
        //     }

        //     return res.status(err.status || 500).render('error', { error: err });
        // });
    }

    protected configureRoutes() {
        const apiComponents = createApiComponents(this.app, this.connection);
        apiComponents.forEach((api) => {
            const path = `/api/${api.path}`;
            console.log(`Listening for ${path}`);
            this.app.use(path, api.configureRoutes());
        });
        // react routing
        this.app.get("*", (req: Request, res: Response): void => {
            res.sendFile(path.resolve("./") + "/build/client/index.html");
        });
    }
}
