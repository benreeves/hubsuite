import { from, Observable, of } from "rxjs";
import { CommunityEvent } from "../types/CommunityEvent";
import { map, mergeMap } from "rxjs/operators";
import axios from "axios";
import { environment } from "../environment";
import moment from "moment";
import { hubService } from "./hub.service";

class EventsService {
    eventsUrl: string;

    constructor() {
        this.eventsUrl = environment.apiUrl + "/events";
    }

    public getCurrentHubEvents(
        startDate?: moment.MomentInput,
        endDate?: moment.MomentInput
    ): Observable<CommunityEvent[]> {
        const currentHub = hubService.getCurrentHub();
        return currentHub.pipe(
            mergeMap((hub) => {
                const sd = startDate
                    ? moment(startDate)
                    : moment().subtract(30, "days");
                const ed = endDate
                    ? moment(endDate)
                    : moment().add(365, "days");
                const promis = axios.get<CommunityEvent[]>(this.eventsUrl, {
                    params: { hubId: hub?.id, startDate: sd, endDate: ed },
                });
                return from(promis).pipe(map((x) => x.data));
            })
        );
    }

    public getHubEvents(
        hubId: string,
        startDate?: moment.MomentInput,
        endDate?: moment.MomentInput
    ): Observable<CommunityEvent[]> {
        const sd = startDate
            ? moment(startDate)
            : moment().subtract(30, "days");
        const ed = endDate ? moment(endDate) : moment().add(365, "days");
        const promis = axios.get<CommunityEvent[]>(this.eventsUrl, {
            params: { hubId: hubId, startDate: sd, endDate: ed },
        });
        return from(promis).pipe(map((x) => x.data));
    }

    public getCommunityEvents(
        communityId: string,
        startDate?: moment.MomentInput,
        endDate?: moment.MomentInput
    ): Observable<CommunityEvent[]> {
        const sd = startDate
            ? moment(startDate)
            : moment().subtract(30, "days");
        const ed = endDate ? moment(endDate) : moment().add(365, "days");
        const promis = axios.get<CommunityEvent[]>(this.eventsUrl, {
            params: { communityId: communityId, startDate: sd, endDate: ed },
        });
        return from(promis).pipe(map((x) => x.data));
    }
}

export const eventsService = new EventsService();
