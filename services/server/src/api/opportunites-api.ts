import { BasicCrudApi, wrapRoute } from "./api-base";
import { Request, Response } from "express";
import { Opportunity } from "../entity/Opportunity";
import { createPrinter } from "typescript";

export class OpportunitySearch {
    type?: string;
    hubId?: string;
}

export class OpportunitiesApi extends BasicCrudApi<Opportunity> {
    configureRoutes() {
        const routes = this.configureDefaultRoutes();
        return routes;
    }

    protected async types(req: Request, res: Response): Promise<void> {
        const types = await this.repo
            .createQueryBuilder("opportunity")
            .select(["opportunity.type"])
            .distinct(true)
            .getRawMany();
        res.json(types);
    }

    protected get(req: Request, res: Response): Promise<void> {
        if (req.params.id && req.params.id == "types") {
            return this.types(req, res);
        }
        return super.get(req, res);
    }

    protected async search(req: Request, res: Response): Promise<void> {
        const where: any = {};
        // validation is for suckers
        const search: OpportunitySearch = { ...req.query };
        let builder = this.repo.createQueryBuilder("opportunity");
        if (search.type) {
            builder = builder.where("opportunity.type = :type", {
                type: search.type,
            });
        }
        if (search.hubId) {
            if (super.checkBadUuid(search.hubId, res)) return;
            builder = builder
                .leftJoin("opportunity.hub", "hub")
                .where("hub.id = :id", { id: search.hubId });
        }

        const ents = await builder.getMany();
        res.json(ents);
    }
}
