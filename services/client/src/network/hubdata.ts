const data = {
    nodes: [
        {
            id: "yycdata",
            category: "data",
            name: "YYC Data Society",
            googleCalendarId:
                "3dtb5r1p3t4lqutirpuljid52g@group.calendar.google.com",
            tags: ["tech", "data"],
            externalUrl: "https://www.yycdata.ca",
            description: "Calgary's home for data science",
            logoUrl: "/YYCDataSociety_Long_Logo.svg",
            primaryColor: null,
        },
        {
            id: "yycdev",
            category: "dev",
            name: "YYC Dev",
            description: `We are a technical meetup group from Calgary, Alberta Canada that meets monthly to connect, learn, and grow as a technical community.

Every month, YYC Dev gathers from the farthest-reaching corners of Calgary's tech-sphere to discuss, learn, share, innovate, and connect fellow developers with one another in a meetup that focuses on building communities and developing software.`,
            externalUrl: "https://www.meetup.com/YYC-dev/",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/5/0/c/e/600_486500686.jpeg",
            tags: ["tech", "dev"],
        },
        {
            id: "yyc-devs",
            category: "dev",
            description: `Calgary's developer and software engineering hub`,
            externalUrl: "https://www.meetup.com/YYC-dev/",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/5/0/c/e/600_486500686.jpeg",
            tags: ["tech", "dev"],
        },
        {
            id: "tech-for-good",
            category: "tech",
            name: "Tech for Good",
            externalUrl: "https://civictechyyc.ca/",
            description:
                "Tech for Good is the hub for social good community projects, most notably Data for Good and Civic Tech YYC",
            tags: ["tech", "social-good"],
        },
        {
            id: "product-calgary",
            category: "product",
            description:
                "Product Calgary (formerly the “Calgary Product Managers Meetup”) is a group of product managers passionate about growing professionally, networking with other product managers, and building the professional community here in Calgary.  ",
            name: "Product Calgary",
            externalUrl: "https://productcalgary.nationbuilder.com/",
            logoUrl:
                "https://media-exp1.licdn.com/dms/image/C560BAQEDJKZTlfUc4w/company-logo_200_200/0/1550339371101?e=2159024400&v=beta&t=ik5vSe-iG6TBbbE_0nPpp_bvd--04u_flTgDk3IDbf8",
            tags: ["tech", "product"],
        },
        {
            id: "calgary-ux",
            name: "Calgary UX",
            category: "product",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/d/5/c/0/600_490314720.jpeg",
            description:
                "We are a community of over 1,500 user experience professionals working together to bring value, empathy and design expertise to everyone around us.",
            externalUrl: "http://calgaryux.com/",
            tags: ["tech", "product", "ui/ux", "design"],
        },
        {
            id: "platform",
            name: "Platform Calgary",
            category: "startup",
            description:
                "Our passion is empowering people // building the next economy.",
            externalUrl: "https://www.platformcalgary.com/",
            logoUrl:
                "https://www.platformcalgary.com/public/images/logo-platform-calgary.svg",
            tags: ["tech", "startup", "product"],
        },
        {
            id: "pydata",
            category: "data",
            name: "PyData Calgary",
            googleCalendarId:
                "90s98s3rfdp5fu0j9fogqjkp30ltshq1@import.calendar.google.com",
            description:
                "PyData is a practitioner oriented meetup focused on learning data science by doing with workshops, code examples, and real life examples. Python oriented but non-exclusive!",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/d/0/9/a/highres_447653402.jpeg",
            primaryColor: "#ed9042",
            externalUrl: "https://www.meetup.com/PyData-Calgary/",
            tags: ["tech", "data", "python", "dev"],
        },
        {
            id: "calgary-r",
            category: "data",
            name: "Calgary R",
            description:
                "CalgaryR  is a community of data enthusiasts and endeavors working with R. We wish to pursue a continuing education/development opportunity from experiences shared by our friends.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/3/e/5/b/600_436335963.jpeg",
            externalUrl: "https://imstatsbee.github.io/calgaryr/",
            tags: ["tech", "data", "R"],
        },
        {
            id: "women-in-data",
            category: "data",
            name: "Women in Data",
            googleCalendarId:
                "b9sgnavfd8mglk9qus6vcc52gdn2oup1@import.calendar.google.com",
            calendarSyncBehaviour: "NoDelete",
            externalUrl: "https://www.meetup.com/Women-In-Data-Calgary/",
            description:
                "The Calgary chapter of Women in Data aims to be a space where women working in data science can connect and learn. We will go in depth into case studies and encourage members to put forth questions related to their work. We also want to grow support for women in tech entrepreneur spaces and promote the tech sector in Calgary.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/6/d/e/b/highres_482428139.jpeg",
            primaryColor: "#d2f1eb",
            tags: ["tech", "data", "women"],
        },
        {
            id: "calgary-ai",
            name: "Calgary AI",
            googleCalendarId:
                "lu9999kq2us5fu81afo3gdvq3pinjl34@import.calendar.google.com",
            externalUrl: "https://www.meetup.com/calgary-ai",
            description:
                "We listen to speakers discuss artificial intelligence, machine learning, and data science, and we also learn from each other. The content is varied, approachable, and valuable, no matter your level of expertise! Open to all.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/5/c/f/3/highres_464363795.jpeg",
            primaryColor: null,
            category: "data",
            tags: ["tech", "data", "women"],
        },
        {
            id: "ada",
            category: "data",
            name: "Alberta Data Architecture",
            googleCalendarId:
                "t1gj79urviiqbpec8rtaec6ch6k78nl0@import.calendar.google.com",
            externalUrl: "http://albertadataarchitecture.org/",
            description:
                "The Alberta Data Architecture (ADA) Community is an informal organization of data professionals that meet\n             regularly to discuss matters of interest to the data community, and to network with like-minded individuals.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/6/5/9/f/highres_484466015.jpeg",
            primaryColor: null,
            tags: ["tech", "data", "women"],
        },
        {
            id: "data-for-good",
            category: "data",
            name: "Data for Good",
            googleCalendarId:
                "8ihpm8js1112hqmdshfat8414vcgbf98@import.calendar.google.com",
            externalUrl: "https://www.meetup.com/Data-For-Good-Calgary/",
            description:
                "Data For Good is a collective of volunteer do-gooders, who want to use their powers for good to help make our communities better through data. We help nonprofit and social organizations harness the power of their data through analytics and visualizations in order to leverage their impact in the community.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/4/8/e/6/highres_479478662.jpeg",
            primaryColor: "#78be28",
            tags: ["tech", "data", "social-good"],
        },
        {
            id: "civic-tech-yyc",
            category: "tech",
            name: "Civic Tech YYC",
            externalUrl: "https://civictechyyc.ca/",
            description:
                "CivicTechYYC is a community based group that is part of a global movement to leverage technology for public good.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/4/3/a/0/600_477737312.jpeg",
            tags: ["tech", "social-good"],
        },
        {
            id: "dama",
            name: "DAMA",
            category: "data",
            googleCalendarId: "info.dama.calgary@gmail.com",
            externalUrl: "http://www.dama-calgary.org/",
            description:
                "DAMA's mission is to provide a non-profit, vendor-independent association where data professionals \ncan go for help and assistance. They aim to To provide the best practice resources such as the DMBoK \nand DM Dictionary of Terms in a mechanism that reaches as many DM professionals as possible and\nto create a trusted environment for DM professionals to collaborate and communicate.\nThey meet the 3rd Thursday of the month at 8AM.\n",
            logoUrl: "/DAMALogo.png",
            primaryColor: null,
            tags: ["tech", "social-good"],
        },
        {
            id: "untapped",
            name: "Untapped Energy",
            googleCalendarId:
                "n07a659vf3raa9m69h4a0a050st0uusi@import.calendar.google.com",
            externalUrl: "https://www.meetup.com/untappedenergy/",
            description:
                "A community for data enthusiasts in the oil and gas industry.",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/b/5/7/6/highres_477046454.jpeg",
            primaryColor: null,
            category: "data",
            tags: ["data", "oil and gas"],
        },
        {
            id: "pyyyc",
            name: "Python YYC",
            category: "dev",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/6/8/b/600_493141675.jpeg",
            externalUrl: "https://www.meetup.com/py-yyc/",
            description:
                "Meet fellow Calgarians interested in Python! Would you like to learn Python, and get into the field? Do you use Python, and want to advance your knowledge? Have you built something interesting in Python, or figured out something tricky, that you want to share with others? Do you just want to chat?",
            tags: ["tech", "dev", "python"],
        },
        {
            id: "yycjs",
            name: "YYC.js",
            category: "dev",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/6/9/d/1/600_449127089.jpeg",
            description:
                "YYC.js is comprised of a group of Calgary web developers and JavaScript aficionados with a passion for open source software. Our passion for software written in JavaScript stems from the ability to be extremely creative due to JavaScript's flexible nature.",
            externalUrl: "https://yycjs.com/",
            tags: ["tech", "dev", "js"],
        },
        {
            id: "dotnet-calgary",
            name: "Calgary .NET Users Group",
            category: "dev",
            logoUrl:
                "https://secure.meetupstatic.com/photos/event/c/4/5/3/600_451430259.jpeg",
            description:
                "The Calgary .NET User Group exists to foster the use and education of the Microsoft .NET platform in Calgary. Anyone can participate in the group, from those that are experienced in .NET to those just starting out.",
            tags: ["tech", "dev", ".net"],
            externalUrl: "https://www.meetup.com/Calgary-net-User-Group/",
        },
        {
            id: "calgary-agile",
            category: "product",
            name: "Calgary Agile Methods User Group",
            logoUrl:
                "https://www.calgaryagile.com/images/calgary-agile-logo.gif",
            description:
                "The Calgary Agile Methods User Group is a meetup group that provides a focal point for the use of agile methods in software development organizations in Calgary.",
            externalUrl: "https://www.calgaryagile.com/",
            tags: ["tech", "product"],
        },
        {
            id: "sdc",
            name: "Software Developers of Calgary Learning Together",
            category: "dev",
            logoUrl: "https://sdc.fyi/static/media/sdc_logo_bold.7f8f9c7d.jpg",
            description:
                "We are now an organization of software developers growing as quickly as we are able. But primarily we host monthly mini-hackathons where we spend a day coding in groups, then towards the end of the day we share what we've learned!",
            externalUrl: "https://sdc.fyi/",
            tags: ["tech", "dev"],
        },
    ],
    links: [
        {
            source: "pydata",
            target: "yycdata",
        },
        {
            source: "calgary-r",
            target: "yycdata",
        },
        {
            source: "data-for-good",
            target: "yycdata",
        },
        {
            source: "women-in-data",
            target: "yycdata",
        },
        {
            source: "untapped",
            target: "yycdata",
        },
        {
            source: "ada",
            target: "yycdata",
        },
        {
            source: "dama",
            target: "yycdata",
        },
        {
            source: "calgary-ai",
            target: "yycdata",
        },
        {
            source: "data-for-good",
            target: "tech-for-good",
        },
        {
            source: "civic-tech-yyc",
            target: "tech-for-good",
        },
        {
            source: "pyyyc",
            target: "yyc-devs",
        },
        {
            source: "yycdev",
            target: "yyc-devs",
        },
        {
            source: "yycjs",
            target: "yyc-devs",
        },
        {
            source: "sdc",
            target: "yyc-devs",
        },
        {
            source: "dotnet-calgary",
            target: "yyc-devs",
        },
        {
            source: "yycdata",
            target: "platform",
        },
        {
            source: "yyc-devs",
            target: "platform",
        },
        {
            source: "data-for-good",
            target: "platform",
        },
        {
            source: "calgary-agile",
            target: "platform",
        },
        {
            source: "civic-tech-yyc",
            target: "platform",
        },
        {
            source: "product-calgary",
            target: "platform",
        },
        {
            source: "calgary-ux",
            target: "platform",
        },
    ],
};

data.nodes.forEach((x) => (x["name"] = x["name"] || x.id));

export default data;
