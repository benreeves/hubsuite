import { ApiBase, BasicCrudApi } from "./api-base";
import express, { Request, Response } from "express";
import { Community } from "../entity/Community";

export class CommunitySearch {
    name?: string;
    city?: string;
    hubId?: string;
}

export class CommunitiesApi extends BasicCrudApi<Community> {
    configureRoutes() {
        return this.configureDefaultRoutes();
    }

    protected async search(req: Request, res: Response): Promise<void> {
        const where: any = {};
        // validation is for suckers
        const search: CommunitySearch = { ...req.query };
        if (search.name) {
            where.name = search.name;
        }
        let builder = this.repo
            .createQueryBuilder("community")
            .leftJoinAndSelect(
                "community.externalResources",
                "externalResource"
            )
            .leftJoinAndSelect("community.contacts", "contact");

        if (search.name) {
            builder = builder.where("LOWER(name) LIKE :name", {
                name: `%${search.name.toLowerCase()}%`,
            });
        }
        if (search.city) {
            builder = builder.where("LOWER(community.city) LIKE :city", {
                city: `%${search.city.toLowerCase()}%`,
            });
        }
        if (search.hubId) {
            if (super.checkBadUuid(search.hubId, res)) return;
            builder = builder
                .leftJoin("community.hubs", "hub")
                .where("hub.id = :id", { id: search.hubId });
        }
        const ents = await builder.getMany();
        res.json(ents);
    }

    protected async get(req: Request, res: Response): Promise<void> {
        const entity = await this.repo.findOne(req.params.id, {
            relations: ["contacts"],
        });
        res.send(entity);
    }
}
