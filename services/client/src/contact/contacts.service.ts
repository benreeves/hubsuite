import axios from "axios";
import { environment } from "../environment";
import { DataOrMessage, extractAxios } from "../shared/axios-util";
import { Contact } from "../types/Contact";

export enum ContactType {
    Community = "community",
    Hub = "hub",
    Sponsor = "sponsor",
}

class ContactsService {
    url: string;

    constructor() {
        this.url = environment.apiUrl + "/contacts";
    }

    public async updateContact(
        id: string,
        patch: Partial<Contact>
    ): Promise<DataOrMessage<Contact>> {
        if (!id) {
            throw new Error("No id provided on contact");
        }
        const res = await axios.put(this.url + "/" + id, patch);
        return extractAxios<Contact>(res);
    }

    public async createContact(
        type: ContactType,
        contact: Contact
    ): Promise<DataOrMessage<Contact>> {
        const body = {
            ...contact,
            type: type,
        };
        const res = await axios.post(this.url, body);
        return extractAxios<Contact>(res);
    }
    public deleteContact(id: string) {}
}

export const contactsService = new ContactsService();
