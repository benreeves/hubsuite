# HubSuite: Spaces for Community Hubs

## Structure

The project contains the express server code in the app directory and the react front end in the client directory. In development, we run the client on the default webpack port 3000 and proxy to the API running on port 8080. This helps with simultaneously debugging the API and the client at the same time and hot reloading each. In produciton builds, the client is prebuilt and served statically.

## Installation

Required tools:

```bash
git clone git@gitlab.com:benreeves/hubsuite.git && cd hubsuite
(cd services/server && npm install)
(cd services/client && npm install)
```

## Run the development server

Start client and server

```bash
npm run dev
```

Server only

```bash
npm start
```

Client only

```bash
npm run client
```

## Containerized development

As part of the effort to move the project into a more maintainable state, some
changes have been made to better support local development and future 
deployment via containers.  These will be fully integrated as work continues.

### Installation

```bash
git clone git@gitlab.com:benreeves/hubsuite.git && cd hubsuite
# Install Gulp, Prettier, and other build tools
npm install
# Create a stub for secrets
cp docker-compose.secrets-example.yaml docker-compose.secrets.yaml 
```

Edit `docker-compose.secrets.yaml` and fill in the necessary values.

Then, install the database by:

```bash
# Start the database
docker compose up postgres
# Install the database from the Heroku dump
npx gulp restore:database --dump=[path]
# Stop the database
docker compose down
```

### Development

Containerized development is in a rough state, without hot reloading, volumes,
and the like.  If you want those, you'll need to look at the non-containerized
instructions.  It is possible to build and run the containers, at least, which
is nice.

```bash
npx gulp init
npx gulp build
npx gulp up
```

When this is complete, you can:
1. See the Hubsuite app by connecting to http://localhost:8080 with your 
   browser.
2. Sync the calendar by running
   `npx gulp test`.

### Gulp supported commands

Gulp commands take the form of:

```bash
npx gulp [command] [arguments]
```

These are the commands currently supported by Gulp:

* (none): runs the `build` command
* `clean`: Remove all build artifacts
* `purge`: Remove all build artifacts, libraries, packages, etc.
* `init`: Run this after the code is checked out to ensure that any dependencies
  needed for development and downloaded, built, created, etc.
* `build`: build all artifacts.
* `build:functions`: build Cloud Functions into containers.
* `build:combined`: build the Hubsuite server/client container.
* `up`: start the containers using Docker Compose, creating any volumes and
networks as needed.
* `down`: stop the containers, delete them, and remove any temporary resources.
Note: the volumes are not deleted.
* `test`: run all tests.
* `buildHeroku`: assemble the files needed to publish to Heroku into the 
  `build` folder in the project root.
* `publishHeroku`: publish the assembled files to Heroku.
* `restore:database --dump=path`: restores the Heroku dump to the database using
  `pg_restore`.

## References

[React Docs](https://reactjs.org/docs/getting-started.html)
[React Material](https://material-ui.com/components/buttons/)
[Express](https://expressjs.com/en/api.html)
[TypeORM](https://typeorm.io/#/)
[RxJs](https://rxjs-dev.firebaseapp.com/guide/overview)
