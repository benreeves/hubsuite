import { Button, Snackbar } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";

export interface SnackProps {
    open: boolean;
    message: string;
    handleClose: (
        event: React.SyntheticEvent | React.MouseEvent,
        reason?: string
    ) => void;
}
export const SnackyBoi = ({ open, message, handleClose }: SnackProps) => {
    return (
        <Snackbar
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
            message={message}
            action={
                <React.Fragment>
                    <Button
                        color="secondary"
                        size="small"
                        onClick={handleClose}
                    >
                        Dismiss
                    </Button>
                    <IconButton
                        size="small"
                        aria-label="close"
                        color="inherit"
                        onClick={handleClose}
                    >
                        <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
            }
        />
    );
};
