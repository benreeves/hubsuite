import { BasicCrudApi } from "./api-base";
import { Request, Response } from "express";
import { Sponsor } from "../entity/Sponsor";

export class SponsorSearch {
    hubId?: string;
}

export class SponsorsApi extends BasicCrudApi<Sponsor> {
    configureRoutes() {
        const routes = this.configureDefaultRoutes();
        return routes;
    }

    protected async search(req: Request, res: Response): Promise<void> {
        const where: any = {};
        // validation is for suckers
        const search: SponsorSearch = { ...req.query };
        let builder = this.repo.createQueryBuilder("sponsor");
        if (search.hubId) {
            if (super.checkBadUuid(search.hubId, res)) return;
            builder = builder
                .leftJoin("sponsor.hub", "hub")
                .where("hub.id = :id", { id: search.hubId });
        }
        const ents = await builder.getMany();
        res.json(ents);
    }
}
