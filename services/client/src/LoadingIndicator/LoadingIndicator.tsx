import {
    CircularProgress,
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        loading: {
            display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            padding: "100px",
        },
    })
);

export const LoadingIndicator = () => {
    const classes = useStyles();
    return (
        <div className={classes.loading}>
            <CircularProgress />
        </div>
    );
};
