#!/usr/bin/env bash

# A simple test for v1 events taken from Google.  This emulates sending a message via Google's PubSub service to
# our locally running Cloud Function.  This is a minimal test and will be replaced with proper service tests later.
#
# The contents are copied-and-pasted from Google's example because our Cloud Function ignores them; we care about the
# receipt of the message and not about the contents.
#
# Note: 'world' base64-encoded is 'd29ybGQ='
curl localhost:8090 \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{
        "context": {
          "eventId":"1144231683168617",
          "timestamp":"2020-05-06T07:33:34.556Z",
          "eventType":"google.pubsub.topic.publish",
          "resource":{
            "service":"pubsub.googleapis.com",
            "name":"projects/sample-project/topics/gcf-test",
            "type":"type.googleapis.com/google.pubsub.v1.PubsubMessage"
          }
        },
        "data": {
          "@type": "type.googleapis.com/google.pubsub.v1.PubsubMessage",
          "attributes": {
             "attr1":"attr1-value"
          },
          "data": "d29ybGQ="
        }
      }'
