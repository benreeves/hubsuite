import { ApiBase, BasicCrudApi } from "./api-base";
import { Request, Response } from "express";
import { Hub } from "src/entity/Hub";

export class HubSearch {
    name?: string;
    city?: string;
    communityId?: string;
}

export class HubsApi extends BasicCrudApi<Hub> {
    configureRoutes() {
        return this.configureDefaultRoutes();
    }

    protected async search(req: Request, res: Response): Promise<void> {
        const where: any = {};
        // validation is for suckers
        const search: HubSearch = { ...req.query };
        if (search.name) {
            where.name = search.name;
        }
        let builder = this.repo.createQueryBuilder("hub");
        if (search.name) {
            builder = builder.where("LOWER(name) LIKE :name", {
                name: `%${search.name.toLowerCase()}%`,
            });
        }
        if (search.city) {
            builder = builder.where("LOWER(hub.city) LIKE :city", {
                city: `%${search.city.toLowerCase()}%`,
            });
        }
        if (search.communityId) {
            builder = builder
                .innerJoin("hub.communities", "community")
                .where("community.id = :communityId", {
                    communityId: search.communityId,
                });
        }

        const ents = await builder.getMany();
        res.json(ents);
    }
}
