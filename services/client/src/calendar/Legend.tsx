import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import "./Legend.css";
import { CalendarEvent } from "./types";

const colors = [
    "#1A535C",
    "#4ECDC4",
    "#DB5375",
    "#FF8C42",
    "#FFE66D",
    "#993955",
    "#C49BBB",
    "#7EBDC2",
    "#C4F4C7",
    "#F9CFF2",
    "#007bff",
];

const defaultPalette: { [key: string]: string } = {
    PyData: colors[0],
    "Calgary R": colors[1],
    "Data for Good": colors[2],
    "Calgary AI": colors[3],
    "Women in Data": colors[4],
    "Untapped Energy": colors[5],
    DAMA: colors[6],
    "Alberta Data Architecture": colors[7],
    "Deep Learning Fellowship": colors[8],
    "Calgary AI Club": colors[9],
    Other: colors[10],
};

function buildPalette(events: CalendarEvent[]) {
    const palette: { [s: string]: string } = {};
    for (let evt of events) {
        if (!(evt.groupName in palette)) {
            if (evt.color) {
                palette[evt.groupName] = evt.color;
            } else {
                const c = colors[Math.floor(Math.random() * colors.length)];
                palette[evt.groupName] = c;
            }
        }
    }
    return palette;
}

type LegendProps = {
    palette: { [key: string]: string };
};

const Legend = ({ palette }: LegendProps) => {
    return (
        <Card className="mt-2">
            <ul className="Legend">
                {Object.keys(palette).map((group) => {
                    let bgColor = palette[group];
                    if (!bgColor) {
                        bgColor =
                            colors[Math.floor(Math.random() * colors.length)];
                    }
                    return (
                        <li className="legend-item" key={group}>
                            <span
                                className="legend-color-box"
                                style={{ backgroundColor: bgColor }}
                            ></span>
                            <span className="legend-label">{group}</span>
                        </li>
                    );
                })}
            </ul>
        </Card>
    );
};

export { Legend, defaultPalette as palette, buildPalette };
