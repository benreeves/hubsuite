import React, { useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Community } from "../types/Community";
import { Link, useHistory } from "react-router-dom";

const useStyles = makeStyles({
    root: {
        height: "30em",
        overflow: "auto",
        display: "grid",
        gridTemplateRows: "1fr auto",
        gridGap: "8px",
    },
    media: {
        height: 180,
    },
    actions: {
        display: "flex",
        // justifyContent: "space-between"
    },
});

export const CommunityCard = ({
    community,
    suppressLink,
}: {
    community: Community;
    suppressLink?: boolean;
}) => {
    let suppressLinks = suppressLink || false;
    const classes = useStyles();
    const history = useHistory();
    const handleOnClick = useCallback(() => {
        if (!suppressLinks) {
            history.push("/app/communities/" + community.id);
        }
    }, [history, community, suppressLinks]);
    const onLinkClick = useCallback(() => {
        if (community.externalUrl) {
            window.open(community.externalUrl);
        } else {
            history.push("/app/communities/" + community.id);
        }
    }, [history, community]);

    return (
        <Card className={classes.root} onClick={handleOnClick}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={community.logoUrl || ""}
                    title="Awesome Community"
                />
                <CardContent style={{ padding: "10px 15px 0px 15px" }}>
                    <Typography variant="h5" component="h1">
                        {community.name}
                    </Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="h4"
                        style={{ maxHeight: 120, overflow: "auto" }}
                    >
                        {community.description}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions className={classes.actions}>
                <Button size="small" color="primary" onClick={onLinkClick}>
                    Learn More
                </Button>
                {/* <Button size="small" color="primary">
                    Share
                </Button> */}
            </CardActions>
        </Card>
    );
};
