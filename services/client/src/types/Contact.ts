export interface Contact {
    id?: string;
    name: string;
    avatar?: string | null;
    email: string;
    type: string;
    parentId: string;
}
