import {
    Divider,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Tooltip,
} from "@material-ui/core";
import DonutLargeIcon from "@material-ui/icons/DonutLarge";
import EventIcon from "@material-ui/icons/Event";
import GroupIcon from "@material-ui/icons/Group";
import EmojiPeopleIcon from "@material-ui/icons/EmojiPeople";
import WorkIcon from "@material-ui/icons/Work";
import HomeIcon from "@material-ui/icons/Home";
import React from "react";
import { Link, useLocation } from "react-router-dom";

export const NavMenuList = () => {
    const loc = useLocation().pathname;

    return (
        <List>
            <ListItem
                button
                key="Home"
                component={Link}
                to="/app/home"
                selected={loc === "/app/home"}
            >
                <Tooltip title="Home">
                    <ListItemIcon>
                        <HomeIcon />
                    </ListItemIcon>
                </Tooltip>
                <ListItemText primary="Home" />
            </ListItem>
            <ListItem
                button
                key="Calendar"
                component={Link}
                to="/app/calendar"
                selected={loc === "/app/calendar"}
            >
                <Tooltip title="Calendar">
                    <ListItemIcon>
                        <EventIcon />
                    </ListItemIcon>
                </Tooltip>
                <ListItemText primary="Calendar" />
            </ListItem>
            <ListItem
                button
                key="Communities"
                component={Link}
                to="/app/communities"
                selected={loc === "/app/communities"}
            >
                <Tooltip title="Communities">
                    <ListItemIcon>
                        <GroupIcon />
                    </ListItemIcon>
                </Tooltip>
                <ListItemText primary="Communities" />
            </ListItem>
            <ListItem
                button
                key="Networking"
                component={Link}
                to="/app/networking"
                selected={loc === "/app/networking"}
            >
                <Tooltip title="Networking">
                    <ListItemIcon>
                        <EmojiPeopleIcon />
                    </ListItemIcon>
                </Tooltip>
                <ListItemText primary="Networking" />
            </ListItem>
            <ListItem
                button
                key="Opportunities"
                component={Link}
                to="/app/opportunities"
                selected={loc === "/app/opportunities"}
            >
                <Tooltip title="Opportunities">
                    <ListItemIcon>
                        <WorkIcon />
                    </ListItemIcon>
                </Tooltip>
                <ListItemText primary="Opportunities" />
            </ListItem>
            <Divider />
            <ListItem button key="All Hubs" component={Link} to="/">
                <Tooltip title="All hubs">
                    <ListItemIcon>
                        <DonutLargeIcon />
                    </ListItemIcon>
                </Tooltip>
                <ListItemText primary="All Hubs" />
            </ListItem>
        </List>
    );
};
