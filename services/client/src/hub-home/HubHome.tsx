import { Network } from "../network/Network";
import { useHubs } from "../shared/hub.hook";
export const HubHome = () => {
    const [hubstate, _] = useHubs();
    const hubId = hubstate.activeHub?.id || "";
    if (!hubId) {
        return null;
    }
    return <Network />;
};
