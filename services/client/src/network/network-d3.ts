import * as d3 from "d3";
import { CommunityNetworkDto, GroupNode } from "./types";

/**
 * Receives a collection of nodes for the d3 network builder and assigns
 * an ordinal group to each node based on the "category" property
 * @param nodes node data structure to assign colors to
 */
function assignOrdinalCategory(nodes: GroupNode[]): any[] {
    const ordinals = new Map<string, number>();
    const mod = [];
    let cur_ord = 1;
    for (let i = 0; i < nodes.length; i++) {
        const node = nodes[i];
        const cat = node.category || (node.tags && node.tags[0]);
        if (cat) {
            let ord = ordinals.get(cat);
            if (!ord) {
                ord = cur_ord;
                ordinals.set(cat, cur_ord);
                cur_ord++;
            }
            mod.push({ ...nodes[i], group: ord });
        } else {
            mod.push({ ...nodes[i], group: i });
        }
    }
    return mod;
}

function drag(simulation: any) {
    function dragstarted(event: any) {
        if (!event.active) simulation.alphaTarget(0.3).restart();
        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
    }

    function dragged(event: any) {
        event.subject.fx = event.x;
        event.subject.fy = event.y;
    }

    function dragended(event: any) {
        if (!event.active) simulation.alphaTarget(0);
        event.subject.fx = null;
        event.subject.fy = null;
    }

    return d3
        .drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
}

export interface NetworkOptions {
    width: number;
    height: number;
}

export interface NetworkState {
    locked: boolean;
    community: GroupNode | null;
    show: boolean;
}

export class NetworkD3Simulation {
    onNodeSelectionChange: (ns: NetworkState) => void = (ns) => {};
    locked: boolean = false;
    community: GroupNode | null = null;
    show: boolean = false;
    simulation: d3.Simulation<any, undefined>;
    nodes: d3.Selection<
        Element | d3.EnterElement | Document | Window | SVGCircleElement | null,
        any,
        SVGGElement,
        unknown
    > | null;
    svgRef: any = null;
    width: number;
    height: number;
    rawData: CommunityNetworkDto | null = null;
    options: NetworkOptions | undefined;
    svg: d3.Selection<SVGSVGElement, unknown, null, undefined>;
    radius: number;
    nodeData: any[] = [];
    linkData: { source: string; target: string }[] = [];
    links: d3.Selection<
        Element | d3.EnterElement | Document | Window | SVGLineElement | null,
        { source: string; target: string },
        SVGGElement,
        unknown
    > | null;
    fresh = true;
    selected: any | null;
    /**
     *
     */
    constructor(options?: NetworkOptions) {
        this.options = options;
        this.svgRef = null;
        this.width = options?.width || 400;
        this.height = options?.height || 400;
        this.radius = 15;
        this.nodes = null;
        this.links = null;
        this.svg = this.initializeSvg();
        this.simulation = d3.forceSimulation();
        this.simulation.stop();
        this.fresh = true;
        this.selected = null;
    }

    /**
     * Kill any existing simulations and run the network sim with the new data
     * @param svgRef d3 selectable referece to the DOM SVG element
     * @param data data to use to build the network
     */
    run(svgRef: any, data: CommunityNetworkDto) {
        this.nodeData = assignOrdinalCategory(data.nodes);
        this.linkData = data.links;
        this.svgRef = svgRef;
        this.kill();
        this.build();
    }

    /**
     * Update the underlying SVG element in the simulation
     * @param svgRef d3 selectable reference to DOM SVG element
     * @param build whether to rebuild the simulation
     */
    updateRef(svgRef: any, build: boolean) {
        this.svgRef = svgRef;
        if (build) {
            this.kill();
            this.build();
        }
    }

    updateData(data: CommunityNetworkDto) {
        this.nodeData = assignOrdinalCategory(data.nodes);
        this.linkData = data.links;
        this.kill();
        this.nodeData = assignOrdinalCategory(data.nodes);
        this.build();
    }

    private build() {
        this.svg = this.initializeSvg();
        this.buildLinks();
        this.buildNodes();
        this.createSimulation();
        this.addListeners();
        this.simulation.restart();
        if (!this.fresh) {
            this.simulation.alpha(1.0);
        }
        this.fresh = false;
    }

    /**
     * Stop the running simulation and clear out all data
     */
    kill() {
        if (this.simulation) {
            this.simulation.force("link", null);
            this.simulation.force("charge", null);
            this.simulation.force("center", null);
            this.simulation.on("tick", null);
            this.simulation.nodes([]);
        }
        this.nodes?.remove();
        this.links?.remove();
        d3.select(this.svgRef).select("svg")?.remove();
        this.simulation.stop();
    }

    /**
     * Initializes the force simulation
     */
    createSimulation() {
        this.simulation
            .nodes(this.nodeData)
            .force(
                "link",
                d3.forceLink(this.linkData).id((d: any) => d.id)
            )
            .force("charge", d3.forceManyBody().strength(-250)) // This adds repulsion between nodes. Play with the -400 for the repulsion strength
            .force("center", d3.forceCenter(this.width / 2, this.height / 2)); // This force attracts nodes to the center of the svg area
    }

    /**
     * Initializes the SVG element to draw the network onto
     */
    initializeSvg() {
        var svg = d3
            .select(this.svgRef)
            .append("svg")
            .style("display", "block")
            .style("margin", "auto")
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", [0, 0, this.width, this.height] as any); // use a viewbox for resizing

        if (this.options?.width) {
            svg = svg.attr("width", this.options.width);
        }
        if (this.options?.height) {
            svg = svg.attr("height", this.options.height);
        }
        return svg;
    }

    buildNodes() {
        const scale = d3.scaleOrdinal(d3.schemeCategory10);
        const colorMap = (d: any) => {
            if (d.color) {
                return d.color;
            } else if (d.primaryColor) {
                return d.primaryColor;
            }
            let cc = scale(d.group || 0);
            return cc;
        };

        // Initialize the nodes
        const nodes = this.svg
            .append("g")
            .attr("stroke", "#fff")
            .attr("stroke-width", 1.5)
            .selectAll("circle")
            .data(this.nodeData)
            .join("circle")
            .attr("r", this.radius)
            .style("fill", colorMap);

        nodes.append("title").text((d: { name: any }) => d.name);
        this.nodes = nodes;
    }

    onMouseEnter(evt: any, node: GroupNode) {
        this.show = true;
        if (!this.locked) {
            this.community = node;
            this.doStateUpdate();
        }
    }

    onClick(evt: any, node: GroupNode) {
        this.locked = !this.locked;
        this.community = node;
        this.doStateUpdate();
    }

    // We don't really need to know that this is a state update, it could be any
    // change to the network state. But, we just call it setState here so I
    // remmeber where to look
    private doStateUpdate() {
        this.onNodeSelectionChange({
            locked: this.locked,
            community: this.community,
            show: this.show,
        });
    }

    buildLinks() {
        // Initialize the links
        this.links = this.svg
            .append("g") // create an svg group
            .attr("stroke", "#999") // set line color
            .attr("stroke-opacity", 0.7)
            .selectAll("line")
            .data(this.linkData)
            .join("line");
    }

    addListeners() {
        this.simulation.on("tick", () => {
            this.nodes
                ?.attr("cx", (d) => {
                    const x = Math.max(
                        this.radius,
                        Math.min(this.width - this.radius, d.x)
                    );
                    return (d.x = x);
                })
                ?.attr("cy", (d) => {
                    return (d.y = Math.max(
                        this.radius,
                        Math.min(this.height - this.radius, d.y)
                    ));
                });

            this.links
                ?.attr("x1", (d: any) => d.source.x)
                ?.attr("y1", (d: any) => d.source.y)
                ?.attr("x2", (d: any) => d.target.x)
                ?.attr("y2", (d: any) => d.target.y);
        });
        this.nodes
            ?.on("mouseover", (event: any, datum: GroupNode) => {
                if (!this.selected) {
                    d3.select(event.target).attr("r", this.radius * 1.2);
                    d3.select(event.target).style("opacity", 0.8);
                }
                this.onMouseEnter(event, datum);
            })
            ?.on("mouseleave", (event: any, datum: GroupNode) => {
                if (event.target !== this.selected) {
                    d3.select(event.target).attr("r", this.radius);
                    d3.select(event.target).style("opacity", 1);
                }
            })
            ?.on("click", (event: any, datum: GroupNode) => {
                // Set node size to be selected
                if (this.selected) {
                    d3.select(this.selected).attr("r", this.radius);
                    d3.select(this.selected).style("opacity", 1);
                    this.selected = null;
                } else {
                    d3.select(event.target).attr("r", this.radius * 1.2);
                    d3.select(event.target).style("opacity", 0.8);
                    this.selected = event.target;
                }
                this.onClick(event, datum);
            })
            ?.call(drag(this.simulation) as any);
    }
}
