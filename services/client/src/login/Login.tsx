import { useState } from "react";

export const Login = () => {
    const [state, setState] = useState(0);
    return <h2 onClick={() => setState(state + 1)}>Login ({state})</h2>;
};
