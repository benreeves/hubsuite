import { Hub } from "./Hub";
import { Locale } from "./Locale";
import { Contact } from "./Contact";
import { Group } from "./Group";

export interface Community extends Group {
    id: string;
    name: string;
    googleCalendarId?: string | null;
    externalUrl?: string | null;
    description: string;
    logoUrl: string | null;
    locale?: Locale | null;
    primaryColor: string | null;
    hubs?: Hub[];
    contacts?: Contact[];
}
