import { Observable, Subscription } from "rxjs";

export function wrapSubscribe<T>(
    source$: Observable<T>,
    nextFn: (value: T) => void
): Subscription {
    return source$.subscribe(nextFn, console.error);
}
