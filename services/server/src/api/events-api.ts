import { BasicCrudApi, wrapRoute } from "./api-base";
import express, { Request, Response } from "express";
import { Sponsor } from "../entity/Sponsor";
import moment from "moment";
import { CommunityEvent } from "../entity/CommunityEvent";

export class EventsSearch {
    hubId?: string;
    communityId?: string;
    minDate?: string;
    maxDate?: string;
}

export class EventsApi extends BasicCrudApi<CommunityEvent> {
    configureRoutes() {
        const router = express.Router();
        router
            .get(
                "/",
                wrapRoute(async (req, res) => await this.search(req, res))
            )
            .get(
                "/:id",
                wrapRoute(async (req, res) => await this.get(req, res))
            );
        return router;
    }

    protected async search(req: Request, res: Response): Promise<void> {
        const where: any = {};
        // validation is for suckers
        const search: EventsSearch = { ...req.query };

        let builder = this.repo
            .createQueryBuilder("evt")
            .leftJoinAndSelect("evt.community", "community");

        if (search.communityId && search.hubId) {
            // this is really just because i'm lazy
            res.status(400);
            res.send({
                error: "Only one of hub id and community id can be specified",
            });
            return;
        }

        if (search.communityId) {
            if (super.checkBadUuid(search.communityId, res)) return;
            builder = builder.where("community.id = :id", {
                id: search.communityId,
            });
        }

        if (search.hubId) {
            if (super.checkBadUuid(search.hubId, res)) return;
            builder = builder
                .leftJoin("community.hubs", "hub")
                .where("hub.id = :id", { id: search.hubId });
        }
        if (search.minDate) {
            builder = builder.where("startDate >= :startDate", {
                startDate: moment(search.minDate).toISOString(),
            });
        }
        if (search.maxDate) {
            builder = builder.where("endDate <= :endDate", {
                endDate: moment(search.maxDate).toISOString(),
            });
        }

        const ents = await builder
            .select(["evt", "community.name", "community.primaryColor"])
            .getMany();
        res.json(ents);
    }
}
