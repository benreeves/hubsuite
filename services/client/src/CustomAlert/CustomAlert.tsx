import { createStyles, makeStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        alert: {
            // display: "block",
            marginLeft: "auto",
            marginRight: "auto",
            padding: "100px",
            // width: "50%",
            backgroundColor: "#F9A38B",
            borderRadius: "10px",
        },
    })
);

export const CustomAlert = () => {
    const classes = useStyles();
    return (
        <div className={classes.alert}>
            There was an error loading the data. Please try again in 30 seconds.
        </div>
    );
};
