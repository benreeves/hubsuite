import { Event } from "react-big-calendar";

export function openLink(link: string) {
    if (!link.match(/^http/)) {
        window.open("http://" + link);
    } else {
        window.open(link);
    }
}
export interface CalendarEvent extends Event {
    location: string;
    groupName: string;
    color?: string;
    link: string;
}
