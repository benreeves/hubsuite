import "./LandingPage.scss";
import { Button } from "@material-ui/core";
import React from "react";

export const Landing = () => {
    return (
        <div className="Landing">
            <header className="Landing-header">
                <img
                    src="/YYCDataSociety_Long_Logo.svg"
                    className="Landing-logo"
                    alt="logo"
                />
                <Button size="large" color="primary" href="/app/home">
                    Join Us
                </Button>
            </header>
        </div>
    );
};
