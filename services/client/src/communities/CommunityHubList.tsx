import React from "react";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import { Hub } from "../types/Hub";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "space-around",
            overflow: "hidden",
            backgroundColor: theme.palette.background.paper,
        },
        gridList: {
            width: "100%",
            // height: 450,
        },
        icon: {
            color: "rgba(255, 255, 255, 0.54)",
        },
    })
);

export default function CommunityHubList({ hubs }: { hubs: Hub[] }) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <GridList cellHeight={180} className={classes.gridList}>
                <GridListTile
                    key="Subheader"
                    cols={2}
                    style={{ height: "auto" }}
                >
                    <ListSubheader component="div">Member of:</ListSubheader>
                </GridListTile>
                {hubs.map((hub) => (
                    <GridListTile key={hub.name}>
                        <img src={hub.logoUrl} alt={hub.name} />
                        <GridListTileBar
                            title={hub.name}
                            subtitle={
                                <span>{hub.strapline || hub.description}</span>
                            }
                            actionIcon={
                                <IconButton
                                    aria-label={`info about ${hub.name}`}
                                    className={classes.icon}
                                >
                                    <InfoIcon />
                                </IconButton>
                            }
                        />
                    </GridListTile>
                ))}
            </GridList>
        </div>
    );
}
