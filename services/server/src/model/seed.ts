import "reflect-metadata";
import { Connection } from "typeorm";
import { seedData } from "./seed-entities";

export async function seed(connection: Connection) {
    try {
        console.log("Inserting a new user into the database...");
        await connection.manager.save(seedData.users);

        console.log("Saving hubs");
        await connection.manager.save(seedData.hubTags);
        await connection.manager.save(seedData.hubs);

        console.log("Saving communities");
        await connection.manager.save(seedData.communityTags);
        await connection.manager.save(seedData.communities);
        await connection.manager.save(seedData.communityContacts);

        console.log("Saving opportunities");
        await connection.manager.save(seedData.oppTags);
        await connection.manager.save(seedData.opportunities);

        console.log("Saving sponsors");
        await connection.manager.save(seedData.sponsors);

        console.log("Saving resources");
        await connection.manager.save(seedData.resources);
    } catch (error) {
        return console.log(error);
    }
}

/**
 * Returns the entites of the database
 */
export async function getEntities(connection: Connection) {
    const entities = [];
    (await connection.entityMetadatas).forEach((x) =>
        entities.push({ name: x.name, tableName: x.tableName })
    );
    return entities;
}

/**
 * Cleans all the entities
 */
export async function cleanAll(entities, connection: Connection) {
    try {
        for (const entity of entities) {
            const repository = await connection.getRepository(entity.name);
            await repository.query(`DELETE FROM ${entity.tableName};`);
        }
    } catch (error) {
        throw new Error(`ERROR: Cleaning test db: ${error}`);
    }
}

export async function resetDb(connection: Connection) {
    try {
        const entities = await getEntities(connection);
        await cleanAll(entities, connection);
    } catch (err) {
        console.log(err);
    }
}
