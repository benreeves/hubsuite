import { createMuiTheme } from "@material-ui/core/styles";

export const Branding = {
    PrimaryColor: "#0082d9;",
    SecondaryColor: "#32B8AA;",
};

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: Branding.PrimaryColor,
        },
        secondary: {
            main: Branding.SecondaryColor,
        },
    },
});
