import { CommunityNetworkDto, GroupNode } from "./types";
import hubData from "./hubdata";
import axios from "axios";
import { environment } from "../environment";
import { Group } from "../types/Group";
import { SupervisorAccountSharp } from "@material-ui/icons";

export interface NodeSearchOptions {
    hubTags?: string[];
    communityTags?: string[];
    inclusiveTags?: boolean;
    name?: string | null;
    depth: number;
    includeParents?: boolean;
    includeChildren?: boolean;
}

export interface NetworkDto {
    nodes: Group[];
    links: { parent: string; child: string; childType: "community" | "hub" }[];
}

function filterNodes(nodes: any[], search: NodeSearchOptions) {
    let filtered = nodes;
    if (!search) {
        return filtered;
    }
    if (search.hubTags && search.hubTags[0]) {
        if (search.inclusiveTags) {
            filtered = nodes.filter((node: { tags: string[] }) =>
                search.hubTags!.some((tag: any) => node.tags.includes(tag))
            );
        } else {
            filtered = nodes.filter((node: { tags: string[] }) =>
                search.hubTags!.every((tag: any) => node.tags.includes(tag))
            );
        }
    }
    if (search.name) {
        filtered = nodes.filter((node: { name: string }) =>
            node.name.toLowerCase().includes(search.name || "")
        );
    }
    return filtered;
}

function filterLinks(links: any[], nodes: GroupNode[]) {
    return links.filter(
        (link: { source: any; target: any }) =>
            nodes.find((el: any) => el.id === link.source) &&
            nodes.find((el: any) => el.id === link.target)
    );
}

export function getNodes_static(
    search: NodeSearchOptions
): Promise<CommunityNetworkDto> {
    const data = JSON.parse(JSON.stringify(hubData));
    let filtered = data.nodes;
    console.log(data);
    let links = data.links;
    if (search.hubTags) {
        filtered = filterNodes(filtered, search);
        links = filterLinks(links, filtered);
    }
    const nodes = filtered.map((el: any) => {
        return {
            ...el,
            name: el.name || el.id,
            googleCalendarId: el.googleCalendarId,
            primaryColor: el.primaryColor,
            description: el.description || "Lorem ipsum doler",
        };
    });
    const d = {
        nodes: nodes,
        links: links,
    };

    return Promise.resolve<CommunityNetworkDto>(d);
}

export async function getSubgraph(
    search: NodeSearchOptions
): Promise<CommunityNetworkDto> {
    const url = environment.apiUrl + "/network";
    const params: any = {
        depth: search.depth,
        inclusiveTags: search.inclusiveTags,
        name: search.name,
    };
    if (search.hubTags) {
        params.hubTags = search.hubTags?.join(",");
    }
    if (search.communityTags) {
        params.communityTags = search.communityTags?.join(",");
    }
    const resp = await axios.get<NetworkDto>(url, { params: params });
    const networkDto = resp.data;
    return {
        nodes: networkDto.nodes,
        links: networkDto.links.map((x) => ({
            source: x.parent,
            target: x.child,
        })),
    };
}

export async function getHubNetwork(
    hubId: string,
    depth: number
): Promise<CommunityNetworkDto> {
    const url = environment.apiUrl + "/network/" + hubId;
    const params = {
        depth: depth,
    };
    const resp = await axios.get<NetworkDto>(url, { params: params });
    const networkDto = resp.data;
    return {
        nodes: networkDto.nodes,
        links: networkDto.links.map((x) => ({
            source: x.parent,
            target: x.child,
        })),
    };
}
