export interface CommunityFormState {
    id?: string;
    isNew: boolean;
    name: string;
    googleCalendarId?: string;
    externalUrl?: string;
    description?: string;
    logoUrl?: string;
    city?: string;
    province?: string;
    country?: string;
    primaryColor?: string;
}
