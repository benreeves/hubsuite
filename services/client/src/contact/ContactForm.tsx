import {
    Box,
    Button,
    createStyles,
    makeStyles,
    TextField,
    Theme,
} from "@material-ui/core";
import React, { useState } from "react";
import { Contact } from "../types/Contact";
import { contactsService, ContactType } from "./contacts.service";
import { SnackyBoi } from "../SnackyBoi/SnackyBoi";
import { DataOrMessage } from "../shared/axios-util";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        contactFormGroup: {
            width: "80%",
            "& > *": {
                margin: "20px 20px 20px 0",
            },
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 250,
        },
    })
);

interface ContactFormProps {
    avatar?: string | null;
    contactId?: string | null;
    name?: string;
    email?: string;
    type: ContactType;
    parentId: string;
    onSave: (payload: DataOrMessage<Contact>) => void;
}

export const ContactForm = ({
    contactId = null,
    avatar = null,
    name = "",
    email = "",
    parentId,
    type,
    onSave = (_) => {},
}: ContactFormProps) => {
    // Setup form
    const isNew = !contactId;
    const classes = useStyles();
    const [formState, setFormState] = useState<Contact>({
        avatar: avatar,
        name: name,
        email: email,
        parentId: parentId,
        type: type,
    });

    const handleInputChange = (
        event: React.ChangeEvent<{ name?: string; value: unknown }>
    ) => {
        const name = event.target.name as keyof Contact;
        const update = { ...formState, [name]: event.target.value };
        setFormState(update);
    };

    // Setup snackbar
    const [snackState, setSnackState] = React.useState({
        open: false,
        message: "",
    });
    const handleClose = (
        event: React.SyntheticEvent | React.MouseEvent,
        reason?: string
    ) => {
        if (reason === "clickaway") {
            return;
        }
        setSnackState({ ...snackState, open: false });
    };
    const handleResponse = (msg: DataOrMessage<Contact>) => {
        if (msg.data) {
            setSnackState({ open: true, message: "Saved!" });
            onSave(msg);
        } else {
            setSnackState({
                open: true,
                message: msg.message || "Something went wrong",
            });
        }
    };

    const submitForm = () => {
        if (formState) {
            if (isNew) {
                contactsService
                    .createContact(type, formState)
                    .then(handleResponse)
                    .catch((err) => {
                        console.log(err);
                    });
            } else {
                contactsService
                    .updateContact(contactId as string, formState)
                    .then(handleResponse)
                    .catch((err) => console.log(err));
            }
        }
    };
    return (
        <form noValidate autoComplete="off">
            <Box flexDirection="column">
                <TextField
                    className={classes.formControl}
                    id="name"
                    name="name"
                    label="Name"
                    onChange={handleInputChange}
                    value={formState.name}
                />
                <Box flexDirection="row" className={classes.contactFormGroup}>
                    <TextField
                        className={classes.formControl}
                        id="email"
                        name="email"
                        label="Email"
                        value={formState.email}
                        onChange={handleInputChange}
                        style={{ width: "40%" }}
                    ></TextField>
                    <TextField
                        id="avatar"
                        className={classes.formControl}
                        name="avatar"
                        label="Avatar"
                        value={formState.avatar || ""}
                        onChange={handleInputChange}
                        style={{ width: "40%" }}
                    ></TextField>
                </Box>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={submitForm}
                >
                    {isNew ? "Add" : "Update"}
                </Button>
            </Box>
            <SnackyBoi
                open={snackState.open}
                handleClose={handleClose}
                message={snackState.message}
            />
        </form>
    );
};
