const Webflow = require("webflow-api");
const axios = require("axios");
const Bottleneck = require("bottleneck/es5");

const webflow = new Webflow({ token: process.env.WEBFLOW_TOKEN });

/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
exports.calsync = async (event, context) => {
    const result = await sync();
    console.log(result);
};

async function sync() {
    try {
        // Pull this out into a function with error handling
        const res = await axios.get(
            `${process.env.CALENDAR_API}?startDate=${new Date(
                new Date().setFullYear(new Date().getFullYear() - 1)
            )}&endDate=${new Date(
                new Date().setFullYear(new Date().getFullYear() + 1)
            )}`
        );
        const events = res.data;

        const existing_items = await webflow.items({
            collectionId: process.env.EVENTS_COLLECTION_ID,
        });
        const existing_by_name = new Set(
            existing_items.items.map((x) => x.name)
        );

        // TODO: How to handle updates? Event organizers can change the name of the event. This
        // technique would cause a duplicate to be written on event name changes. There is a source
        // id which could be mapped and persisted to the CMS instead of the name
        const cmsItems = [];
        for (let i = 0; i < events.length; i++) {
            let evt = events[i];
            if (existing_by_name.has(evt.name)) continue;
            // Filter out DAMA board meetings
            if (evt.name.includes("DAMA Calgary Board Meeting")) continue;
            const cmsItem = createCMSItem(evt);
            cmsItems.push(cmsItem);
        }

        // one request per second since the API throttles you to 60/min
        const limiter = new Bottleneck({
            minTime: 1000,
        });
        const resArr = await limiter.schedule(() => {
            const allTasks = cmsItems.map((item) => syncItem(item));
            return Promise.all(allTasks);
        });

        // If this happens, we should roll back the created items
        try {
            await publishSite();
        } catch (err) {
            return {
                message: "Error occured publishing site, items only staged",
                error: err,
            };
        }

        // Extract a summary of the sync
        let success = 0;
        const messages = [];
        for (let i = 0; i < resArr.length; i++) {
            success += resArr[i].ok ? 1 : 0;
            if (!resArr[i].ok) {
                messages.push(resArr[i].err.message);
            }
        }

        return {
            message: `Synced ${
                resArr.length
            } events with ${success} completions and ${
                resArr.length - success
            } failures.`,
            error: null,
        };
    } catch (err) {
        console.log(err);
        return { message: "Critical error occured", error: err };
    }
}

function createCMSItem(evt) {
    const name = evt.name;
    const slug = str_to_slug(name);
    const startDate = evt.startDate;
    const endDate = evt.endDate;
    const description = evt.description;
    const link = evt.link;
    const community = evt.community.name;
    const primaryColor = evt.community.primaryColor;

    return {
        collectionId: process.env.EVENTS_COLLECTION_ID,
        fields: {
            name: name,
            slug: slug,
            "event-date-start": startDate,
            "event-date-time-end": endDate,
            "summary-of-the-event": description,
            "full-description": description,
            "event-signup-link": link,
            "event-organizer": community,
            "event-featured-color": primaryColor,
            _archived: false,
            _draft: false,
        },
    };
}

async function syncItem(cmsItem) {
    try {
        const res = await webflow.createItem(cmsItem);
        // handle response message?
        return { ok: true, err: null };
    } catch (err) {
        console.log(err);
        return { ok: false, err: err };
    }
}

async function publishSite() {
    // Publish twice, once to subdomain once to primary. Maybe
    // this fixes the publish issues?
    await webflow.publishSite({
        siteId: process.env.WEBFLOW_SITE_ID,
        domains: [process.env.WEBFLOW_SITE_SUBDOMAIN],
    });
    await webflow.publishSite({
        siteId: process.env.WEBFLOW_SITE_ID,
        domains: [process.env.WEBFLOW_SITE_DOMAIN],
    });
}

// Webflow renames things, replicate their logic
const str_to_slug = (str) => {
    // Strip whitespace at begin and end
    str = str.replace(/^\s+|\s+$/g, "");
    str = str.toLowerCase();

    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++)
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));

    str = str
        .replace(/[^a-z0-9 -]/g, "") // Remove any non alphanumeric
        .replace(/\s+/g, "-") // replace space with -
        .replace(/-+/g, "-"); // replace multiple - with single -

    return str;
};
