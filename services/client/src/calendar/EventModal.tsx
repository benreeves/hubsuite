import React from "react";
import {
    createStyles,
    makeStyles,
    Theme,
    useTheme,
} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { TransitionProps } from "@material-ui/core/transitions";
import moment from "moment";
import { CalendarEvent } from "./types";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            position: "relative",
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1,
        },
    })
);

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement },
    ref: React.Ref<unknown>
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export const FullScreenDialog = ({
    event,
    open,
    onClose,
}: {
    event: CalendarEvent;
    open: boolean;
    onClose: () => void;
}) => {
    const classes = useStyles();

    return (
        <Dialog open={open} onClose={onClose} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        edge="start"
                        color="inherit"
                        onClick={onClose}
                        aria-label="close"
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        {event.title}
                    </Typography>
                    <Button autoFocus color="inherit" onClick={onClose}>
                        Close
                    </Button>
                </Toolbar>
            </AppBar>
            <List>
                {event.title && (
                    <ListItem button>
                        <ListItemText
                            primary="Event Name"
                            secondary={event.title}
                        />
                    </ListItem>
                )}
                <Divider />
                {event.location && (
                    <ListItem button>
                        <ListItemText
                            primary="Event Location"
                            secondary={event.location}
                        />
                    </ListItem>
                )}
                {event.groupName && (
                    <ListItem button>
                        <ListItemText
                            primary="Hosted By"
                            secondary={event.groupName}
                        />
                    </ListItem>
                )}
                {event.link && (
                    <ListItem button>
                        <ListItemText
                            primary="Event Link"
                            secondary={event.link}
                        />
                    </ListItem>
                )}
                {event.start && (
                    <ListItem button>
                        <ListItemText
                            primary="Event Start"
                            secondary={moment(event.start).format(
                                "dddd, MMMM Do YYYY, h:mm a"
                            )}
                        />
                    </ListItem>
                )}
                {event.end && (
                    <ListItem button>
                        <ListItemText
                            primary="Event End"
                            secondary={moment(event.end).format(
                                "dddd, MMMM Do YYYY, h:mm a"
                            )}
                        />
                    </ListItem>
                )}
            </List>
        </Dialog>
    );
};
