import { useEffect, useState } from "react";
import { Observable, Subscription } from "rxjs";
import { Hub } from "../types/Hub";
import { hubService } from "./hub.service";
import { wrapSubscribe } from "./utility";

interface HubState {
    hubs: Hub[];
    activeHub: Hub | null;
}

/**
 * Push based view model for hub data
 */
export function useHubs(): [HubState, Function] {
    const setCurrentHub = (id: string) => hubService.setCurrentHub(id);
    const [state, setState] = useState<HubState>({ hubs: [], activeHub: null });

    /**
     * Manage subscriptions with auto-cleanup
     */
    useEffect(() => {
        const subscriptions: Subscription[] = [
            wrapSubscribe<Hub[]>(hubService.getUserHubs(), (hubs) =>
                setState((state) => ({ ...state, hubs }))
            ),
            wrapSubscribe<Hub>(hubService.getCurrentHub(), (active) =>
                setState((state) => ({ ...state, activeHub: active }))
            ),
        ];

        return () => {
            subscriptions.map((it) => it.unsubscribe());
        };
    }, []);

    return [state, setCurrentHub];
}
