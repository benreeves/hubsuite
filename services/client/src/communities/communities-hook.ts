import { useEffect, useState } from "react";
import { Observable, Subscription } from "rxjs";
import { wrapSubscribe } from "../shared/utility";
import { Community } from "../types/Community";
import { communitiesService } from "./communities-service";

interface CommunityState {
    allCommunities: Community[];
    currentCommunities: Community[];
}

/**
 * Push based view model for communities data
 */
export function useCommunitiesFacade(): CommunityState {
    const [state, setState] = useState<CommunityState>({
        allCommunities: [],
        currentCommunities: [],
    });

    /**
     * Manage subscriptions with auto-cleanup
     */
    useEffect(() => {
        const subscriptions: Subscription[] = [
            wrapSubscribe<Community[]>(
                communitiesService.getAllCommunities(),
                (cs) => setState((state) => ({ ...state, allCommunities: cs }))
            ),
            wrapSubscribe<Community[]>(
                communitiesService.getCurrentCommunities(),
                (cs) =>
                    setState((state) => ({ ...state, currentCommunities: cs }))
            ),
        ];

        return () => {
            subscriptions.map((it) => it.unsubscribe());
        };
    }, []);

    return state;
}
