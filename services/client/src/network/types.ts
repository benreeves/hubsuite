import { Group } from "../types/Group";

export interface GroupNode extends Group {
    category?: string;
    tags?: string[];
}

export interface CommunityNetworkDto {
    nodes: GroupNode[];
    links: { source: string; target: string }[];
}
