import { BehaviorSubject, from, Observable, of } from "rxjs";
import { Hub } from "../types/Hub";
import { map } from "rxjs/operators";
import axios from "axios";
import { environment } from "../environment";
import { axiosGet, DataOrMessage } from "./axios-util";

export interface HubSearchOptions {
    name?: string;
    city?: string;
    communityId?: string;
}
export function searchHubs(
    options: HubSearchOptions
): Promise<DataOrMessage<Hub[]>> {
    return axiosGet<Hub[]>(environment.apiUrl + "/hubs", {
        params: { ...options },
    });
}

class HubService {
    hubsUrl: string;

    private selectedHub = new BehaviorSubject<Hub>({} as Hub);

    constructor() {
        this.hubsUrl = environment.apiUrl + "/hubs";
        this.initialize();
    }

    private initialize() {
        const promis = axios.get<
            { name: string; id: string; logoUrl: string }[]
        >(this.hubsUrl, { params: { name: "yyc data" } });
        from(promis)
            .pipe(map((x) => x.data[0]))
            .subscribe((data) => this.selectedHub.next(data));
    }

    public getCurrentHub(): Observable<Hub> {
        return this.selectedHub.asObservable();
    }

    public setCurretHub(hub: Hub): void {
        this.selectedHub.next(hub);
    }

    public getUserHubs(): Observable<Hub[]> {
        return of([
            {
                name: "YYC Data Community",
                id: "1",
                logoUrl: "/YYCDataSociety_Logo.svg",
            },
            { name: "YYC Dev", id: "2" },
        ]);
    }

    public setCurrentHub(id: string): Observable<boolean> {
        return of(true);
    }
}

export const hubService = new HubService();
