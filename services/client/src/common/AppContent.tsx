import {
    Theme,
    createStyles,
    CssBaseline,
    WithStyles,
    withStyles,
    makeStyles,
} from "@material-ui/core";
import React from "react";
import AppLayout from "./AppLayout";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
        },
        // necessary for content to be below app bar
        toolbar: theme.mixins.toolbar,
        content: {
            flexGrow: 1,
            padding: theme.spacing(0),
            [theme.breakpoints.up("sm")]: {
                padding: theme.spacing(0),
            },
        },
    })
);

interface ComponentProps extends React.ComponentProps<any> {}

export const AppContent = ({ children }: ComponentProps) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <AppLayout />
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {children}
            </main>
        </div>
    );
};
