import "./App.scss";
import React from "react";
import { ThemeProvider } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import { theme } from "./branding";
import { Route, Switch } from "react-router-dom";
import { Landing } from "./landing/LandingPage";
import { NetworkingPage } from "./networking/NetworkingPage";
import { OpportunitiesPage } from "./opportunities/OpportunitiesPage";
import { CommunityDetail } from "./communities/CommunityDetail";
import { AppContent } from "./common/AppContent";
import { CommunitiesPage } from "./communities/CommunitiesPage";
import { HubHome } from "./hub-home/HubHome";
import { Login } from "./login/Login";
import { CalendarPage } from "./calendar/CalendarPage";
import { PageNotFound } from "./notfound/PageNotFound";

function App() {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Switch>
                <Route path="/login" component={Login} />
                <Route exact path={["/app/:path?/:id?"]}>
                    <AppContent>
                        <Switch>
                            <Route path="/app/communities/:id">
                                <CommunityDetail />
                            </Route>
                            <Route path="/app/communities">
                                <CommunitiesPage />
                            </Route>
                            <Route path="/app/networking">
                                <NetworkingPage />
                            </Route>
                            <Route path="/app/home">
                                <HubHome />
                            </Route>
                            <Route path="/app/opportunities">
                                <OpportunitiesPage />
                            </Route>
                            <Route path="/app/calendar">
                                <CalendarPage />
                            </Route>
                            <Route path="*">
                                <PageNotFound />
                            </Route>
                        </Switch>
                    </AppContent>
                </Route>
                <Route exact path="/">
                    <Landing />
                </Route>
                <Route path="*">
                    <PageNotFound />
                </Route>
            </Switch>
        </ThemeProvider>
    );
}

export default App;
