require("dotenv").config();
const isDev = process.env.NODE_ENV === "development";
const watchDev = process.env.WATCH_DEV === "watch";
const entitiesDir = watchDev
    ? ["src/entity/**/*.ts"]
    : ["build/src/entity/*.js"];
module.exports = {
    type: "postgres",
    host: process.env.TYPEORM_HOST,
    port: parseInt(process.env.TYPEORM_PORT, 10) || 5432,
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    migrations: ["src/migration/**/*.ts"],
    entities: entitiesDir,
    subscribers: ["src/subscriber/**/*.ts"],
    cli: {
        migrationsDir: "src/migrations",
        entitiesDir: "src/entity",
        subscribersDir: "src/subscriber",
    },
    migrationsRun: true,
    logging: true,
    ssl: !isDev,
    synchronize: true,
    extra: !isDev ? { ssl: { rejectUnauthorized: false } } : undefined,
};
