import React from "react";
import { EventProps } from "react-big-calendar";
import { FullScreenDialog } from "./EventModal";
import { CalendarEvent } from "./types";

export const CalendarViewEvent = (props: EventProps<CalendarEvent>) => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = (isopen: boolean) => {
        console.log("handling open", isopen);
        setOpen(isopen);
    };

    return (
        <div>
            <FullScreenDialog
                event={props.event}
                open={open}
                onClose={() => handleOpen(false)}
            ></FullScreenDialog>
            <div onClick={(e) => handleOpen(true)}>
                <span>
                    <strong>{props.title} </strong>
                </span>
            </div>
        </div>
    );
};
