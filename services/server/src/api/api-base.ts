import "reflect-metadata";
import express, { Router } from "express";
import { Connection, DeleteResult, Repository } from "typeorm";
import { Request, Response } from "express";
import { rejects } from "assert";

const uuidRegex =
    /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
export type ObjectType<T> = { new (): T } | Function;

export const wrapRoute = (fn) => async (req, res, next) => {
    try {
        // run controllers logic
        await fn(req, res, next);
    } catch (e) {
        // if an exception is raised, do not send any response
        // just continue performing the middleware chain
        next(e);
    }
};

export abstract class ApiBase {
    private _path: string;

    constructor(protected app: express.Application, path: string) {
        this._path = path;
    }
    public get path() {
        return this._path;
    }

    abstract configureRoutes(): Router;
}

export abstract class BasicCrudApi<T> extends ApiBase {
    repo: Repository<T>;
    constructor(
        protected app: express.Application,
        private type: ObjectType<T>,
        name: string,
        protected connection: Connection
    ) {
        super(app, name);
        this.repo = this.createRepository();
    }

    protected createRepository(): Repository<T> {
        return this.connection.getRepository(this.type);
    }

    protected async search(req: Request, res: Response): Promise<void> {
        const entities = await this.repo.find();
        res.json(entities);
    }

    protected async get(req: Request, res: Response): Promise<void> {
        const entity = await this.repo.findOne(req.params.id);
        res.send(entity);
    }

    protected async create(req: Request, res: Response): Promise<void> {
        const entity = await this.repo.create(req.body);
        const results = await this.repo.save(entity as any);
        res.send(results);
    }

    protected async merge(req: Request, res: Response): Promise<void> {
        const entity = await this.repo.findOne(req.params.id);
        this.repo.merge(entity, req.body);
        const results = await this.repo.save(entity as any);
        res.send(results);
    }

    protected async delete(req: Request, res: Response): Promise<void> {
        const results = await this.repo.delete(req.params.id);
        res.send(results);
    }

    protected configureDefaultRoutes(): Router {
        const router = express.Router();
        router
            .get(
                "/",
                wrapRoute(async (req, res) => await this.search(req, res))
            )
            .get(
                "/:id",
                wrapRoute(async (req, res) => await this.get(req, res))
            )
            .post(
                "/",
                wrapRoute(async (req, res) => await this.create(req, res))
            )
            .put(
                "/:id",
                wrapRoute(async (req, res) => await this.merge(req, res))
            )
            .delete(
                "/:id",
                wrapRoute(async (req, res) => await this.delete(req, res))
            );
        return router;
    }

    // does this work lol?
    protected configureTransactionScoped(router: Router) {
        router.use(async (req, res, next) => {
            this.connection.transaction((txMgr) => {
                return new Promise<void>((resolve, reject) => {
                    req["txMgr"] = txMgr;
                    res.on("finish", () => resolve());
                    res.on("error", (err) => reject(err));
                    next();
                });
            });
        });
    }

    protected checkBadUuid(uuid: string, res: Response): boolean {
        if (!uuidRegex.test(uuid)) {
            res.status(400);
            res.send({ error: "Invalid uuid" });
            return true;
        }
        return false;
    }
}
