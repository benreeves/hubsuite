import { Box, createStyles, makeStyles, Theme } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import React, { useState } from "react";
import { ContactCard } from "../contact/ContactCard";
import { Contact } from "../types/Contact";
import { ContactForm } from "../contact/ContactForm";
import { DataOrMessage } from "../shared/axios-util";
import { ContactType } from "../contact/contacts.service";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        contactBar: {
            alignItems: "center",
            "& > h4": {
                paddingRight: "1em",
            },
        },
        contactCard: {
            width: "50%",
            padding: "10px 0px",
        },
    })
);

interface ContactListProps {
    contacts: Contact[];
    onEmit: (contacts: Contact[]) => void;
    parentId: string;
}

export const ContactList = ({
    contacts,
    onEmit,
    parentId,
}: ContactListProps) => {
    const [showAddContactForm, setShowAddContactForm] = useState(false);
    const classes = useStyles();

    // --Event handlers--
    // Whether to show add contact form
    const handleAddContactClick = () => {
        setShowAddContactForm(!showAddContactForm);
    };
    // After API call
    const onContactAdded = (payload: DataOrMessage<Contact>) => {
        if (payload.data) {
            onEmit([...contacts, payload.data]);
        } else {
            console.log("no data");
        }
    };

    return (
        <div className={classes.root}>
            <Box
                display="flex"
                flexDirection="row"
                className={classes.contactBar}
            >
                <h4>Get in touch with us!</h4>
                <AddIcon onClick={handleAddContactClick} />
            </Box>
            <Box display="flex" flexDirection="column">
                {contacts.map((contact, i) => (
                    <div className={classes.contactCard} key={i}>
                        <ContactCard
                            name={contact.name}
                            avatar={contact.avatar || ""}
                            email={contact.email}
                        />
                    </div>
                ))}
            </Box>
            {showAddContactForm ? (
                <ContactForm
                    type={ContactType.Community}
                    onSave={onContactAdded}
                    parentId={parentId}
                />
            ) : null}
        </div>
    );
};
