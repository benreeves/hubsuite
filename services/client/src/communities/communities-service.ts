import { from, Observable, of } from "rxjs";
import { Community } from "../types/Community";
import { CommunityFormState } from "./CommunityFormState";
import { map, switchMap } from "rxjs/operators";
import axios from "axios";
import { environment } from "../environment";
import { hubService, searchHubs } from "../shared/hub.service";
import { DataOrMessage } from "../shared/axios-util";
import { Hub } from "../types/Hub";

export function mergeFormState(form: CommunityFormState, existing?: Community) {
    return {
        ...(existing || {}),
        name: form.name,
        description: form.description,
        googleCalendarId: form.googleCalendarId,
        logoUrl: form.logoUrl,
        primaryColor: form.primaryColor,
        locale: {
            city: form.city,
            province: form.province,
            country: form.country,
        },
    };
}

export function toFormState(community: Community): CommunityFormState {
    return {
        ...community,
        isNew: false,
        country: community.locale?.country,
        city: community.locale?.city,
        province: community.locale?.province,
        googleCalendarId: community.googleCalendarId || "",
        primaryColor: community.primaryColor || "",
        description: community.description || "",
        externalUrl: community.externalUrl || "",
        logoUrl: community.logoUrl || "",
    };
}

class CommunitiesService {
    url: string;

    constructor() {
        this.url = environment.apiUrl + "/communities";
    }

    public getAllCommunities(): Observable<Community[]> {
        const promis = axios.get<Community[]>(this.url);
        return from(promis).pipe(map((x) => x.data));
    }

    public getCommunity(id: string): Observable<Community> {
        const promis = axios.get<Community>(this.url + "/" + id);
        return from(promis).pipe(map((x) => x.data));
    }

    public getHubsForCommunity(id: string): Promise<DataOrMessage<Hub[]>> {
        return searchHubs({ communityId: id });
    }

    public async saveForm(
        formState: CommunityFormState
    ): Promise<{ ok: boolean; err: string | null }> {
        const record = mergeFormState(formState);
        if (formState.isNew) {
            const res = await axios.post(this.url, record);
            if (res.status < 400) {
                return { ok: true, err: null };
            } else {
                return { ok: false, err: `Something went wrong: ${res.data}` };
            }
        } else {
            const res = await axios.put(this.url + "/" + formState.id, record);
            if (res.status < 400) {
                return { ok: true, err: null };
            } else {
                return { ok: false, err: `Something went wrong: ${res.data}` };
            }
        }
    }

    public getCurrentCommunities(): Observable<Community[]> {
        return hubService.getCurrentHub().pipe(
            switchMap((res) =>
                axios.get<Community[]>(this.url, { params: { hubId: res.id } })
            ),
            map((x) => x.data)
        );
    }
}

export const communitiesService = new CommunitiesService();
