"use strict";

const cp = require("child_process");
const fs = require("fs");
const gulp = require("gulp");

const yargs = require("yargs/yargs");
const { hideBin } = require("yargs/helpers");
const argv = yargs(hideBin(process.argv)).argv;

function missingEnvironmentVariable(envVarName) {
    throw `Missing environment variable ${envVarName}`;
}

function cleanTask(cb) {
    cp.spawnSync("rm", ["-Rf", "build"], {
        cwd: ".",
        stdio: "inherit",
    });
    cb();
}

function purgeTask(cb) {
    cleanTask(cb);

    cp.spawnSync("rm", ["-Rf", "node_modules"], {
        cwd: ".",
        stdio: "inherit",
    });
    cp.spawnSync("rm", ["-Rf", "node_modules"], {
        cwd: "services/client",
        stdio: "inherit",
    });
    cp.spawnSync("rm", ["-Rf", "build", "node_modules"], {
        cwd: "services/server",
        stdio: "inherit",
    });
    cp.spawnSync("rm", ["-Rf", "node_modules"], {
        cwd: "services/yycdata-calsync-legacy-1/src",
        stdio: "inherit",
    });
    cb();
}

function initFunctionsTask(cb) {
    // Install the libraries needed for local dev and testing
    cp.spawnSync("npm", ["install"], {
        cwd: "services/yycdata-calsync-legacy-1/src",
        stdio: "inherit",
    });
    cb();
}

function buildCombinedImageTask(cb) {
    buildHerokuTask(cb);

    // Build the container
    cp.spawnSync(
        "pack",
        [
            "build",
            "--builder",
            "heroku/buildpacks:20",
            "--buildpack",
            "heroku/nodejs",
            "hubsuite-combined",
        ],
        {
            cwd: "build/hubsuite-combined",
            stdio: "inherit",
        }
    );

    cb();
}

function buildFunctionsTask(cb) {
    // Build the container
    cp.spawnSync(
        "pack",
        [
            "build",
            "--builder",
            "gcr.io/buildpacks/builder:v1",
            "--env",
            "GOOGLE_RUNTIME=nodejs",
            "--env",
            "GOOGLE_FUNCTION_SIGNATURE_TYPE=event",
            "--env",
            "GOOGLE_FUNCTION_TARGET=calsync",
            "yycdata-calsync-legacy-1",
        ],
        {
            cwd: "services/yycdata-calsync-legacy-1/src",
            stdio: "inherit",
        }
    );

    cb();
}

function runDockerComposeUpTask(cb) {
    cp.spawn(
        "docker",
        [
            "compose",
            "-f",
            "docker-compose.yaml",
            "-f",
            "docker-compose.secrets.yaml",
            "up",
        ],
        { stdio: "inherit" }
    );
    cb();
}

function runDockerComposeDownTask(cb) {
    cp.spawn(
        "docker",
        [
            "compose",
            "-f",
            "docker-compose.yaml",
            "-f",
            "docker-compose.secrets.yaml",
            "down",
        ],
        { stdio: "inherit" }
    );
    cb();
}

function testFunctionsTask() {
    return cp.spawn("bash", ["test.sh"], {
        cwd: "services/yycdata-calsync-legacy-1/tests",
        stdio: "inherit",
    });
}

function buildHerokuTask(cb) {
    cp.spawn("cp", ["-R", "services/server", "build/hubsuite-combined"], {
        cwd: ".",
        stdio: "inherit",
    });
    cp.spawn(
        "cp",
        ["-R", "services/client", "build/hubsuite-combined/client"],
        {
            cwd: ".",
            stdio: "inherit",
        }
    );
    cb();
}

// This is a temporary task used to ensure deployments to Heroku keep working as
// they have previously.
// See: https://gitlab.com/benreeves/hubsuite/-/issues/51
function publishHerokuTask(cb) {
    const HEROKU_APP_PROD =
        process.env.HEROKU_APP_PROD ||
        missingEnvironmentVariable("HEROKU_APP_PROD");
    const HEROKU_API_KEY =
        process.env.HEROKU_API_KEY ||
        missingEnvironmentVariable("HEROKU_API_KEY");

    if (!fs.existsSync("./build/hubsuite-combined")) {
        throw "Heroku build not found.  Run `npx gulp buildHeroku` first.";
    }

    cp.spawn(
        "dpl",
        [
            "--provider=heroku",
            `--app=${HEROKU_APP_PROD}`,
            `--api-key=${HEROKU_API_KEY}`,
            "--skip_cleanup",
        ],
        {
            cwd: "build/hubsuite-combined",
            stdio: "inherit",
        }
    );

    cb();
}

function restoreDatabaseTask(cb) {
    if (!argv.dump) {
        throw "Dumpfile not provided";
    }
    const dumpFile = argv.dump;

    cp.spawn(
        "pg_restore",
        [
            "--verbose",
            "--clean",
            "--no-acl",
            "--no-owner",
            "-h",
            "localhost",
            "-U",
            "hubsuite",
            "-d",
            "public",
            dumpFile,
        ],
        {
            cwd: "build",
            stdio: "inherit",
        }
    );

    cb();
}

exports.clean = cleanTask;
exports.purge = purgeTask;

exports.init = initFunctionsTask;

exports["build:functions"] = buildFunctionsTask;
exports["build:combined"] = buildCombinedImageTask;
exports.build = gulp.parallel(
    exports["build:functions"],
    exports["build:combined"]
);

exports.up = runDockerComposeUpTask;
exports.down = runDockerComposeDownTask;

exports.test = testFunctionsTask;

exports.buildHeroku = buildHerokuTask;
exports.publishHeroku = publishHerokuTask;

exports["restore:database"] = restoreDatabaseTask;

exports.default = gulp.series(exports.build);
