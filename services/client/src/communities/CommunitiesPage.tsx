import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { useCommunitiesFacade } from "./communities-hook";
import { CommunityCard } from "./CommunityCard";
import { LoadingIndicator } from "../LoadingIndicator/LoadingIndicator";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.text.secondary,
        },
    })
);

export const CommunitiesPage = () => {
    const classes = useStyles();
    const state = useCommunitiesFacade();

    return state.currentCommunities[0] ? (
        <div className={classes.root}>
            <Grid container spacing={3} alignItems="stretch">
                {state.currentCommunities.map((el) => (
                    <Grid item xs={12} sm={6} md={4}>
                        <CommunityCard community={el} />{" "}
                    </Grid>
                ))}
            </Grid>
        </div>
    ) : (
        <LoadingIndicator />
    );
};
