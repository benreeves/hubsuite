import { Tag } from "src/entity/Tag";
import { Group } from "../entity/Group";

export interface NetworkDtoGroup extends Group {
    tags: string[];
}
export interface NetworkDto {
    nodes: NetworkDtoGroup[];
    links: { parent: string; child: string; childType: "community" | "hub" }[];
}
