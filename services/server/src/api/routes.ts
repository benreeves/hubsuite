import { Connection } from "typeorm";
import { ApiBase } from "./api-base";
import { CommunitiesApi } from "./communities-api";
import { HubsApi } from "./hubs-api";
import { OpportunitiesApi } from "./opportunites-api";
import { SponsorsApi } from "./sponsors-api";
import { EventsApi } from "./events-api";
import { ContactsApi } from "./contacts-api";
import { NetworkApi } from "./network-api";
import express from "express";
import { Community } from "../entity/Community";
import { Hub } from "../entity/Hub";
import { Opportunity } from "../entity/Opportunity";
import { Sponsor } from "../entity/Sponsor";
import { CommunityEvent } from "../entity/CommunityEvent";
import { Contact } from "../entity/Contact";

export function createApiComponents(
    app: express.Application,
    connection: Connection
): ApiBase[] {
    const apiComponents = [
        new CommunitiesApi(app, Community, "communities", connection),
        new HubsApi(app, Hub, "hubs", connection),
        new OpportunitiesApi(app, Opportunity, "opportunities", connection),
        new SponsorsApi(app, Sponsor, "sponsors", connection),
        new EventsApi(app, CommunityEvent, "events", connection),
        new ContactsApi(app, Contact, "contacts", connection),
        new NetworkApi(app, connection),
    ];
    return apiComponents;
}
