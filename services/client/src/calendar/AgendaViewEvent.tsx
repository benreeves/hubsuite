import React from "react";
import { Button } from "@material-ui/core";
import { EventProps } from "react-big-calendar";
import { FullScreenDialog } from "./EventModal";
import { CalendarEvent, openLink } from "./types";

export const AgendaViewEvent = (props: EventProps<CalendarEvent>) => {
    const [open, setOpen] = React.useState(false);

    const handleOpen = (isopen: boolean) => {
        setOpen(isopen);
    };

    return (
        <div className="yyc-agenda-event">
            <div onClick={() => handleOpen(true)}>
                <b>{props.title}</b>
                <div>
                    Presented by:{" "}
                    <span>
                        <b>{props.event.groupName}</b>
                    </span>
                </div>
                <div>
                    Location:{" "}
                    <span>
                        <b>{props.event.location}</b>
                    </span>
                </div>
                <div>
                    <Button
                        color="secondary"
                        onClick={() => openLink(props.event.link)}
                    >
                        Link
                    </Button>
                </div>
            </div>
            <FullScreenDialog
                event={props.event}
                open={open}
                onClose={() => handleOpen(false)}
            ></FullScreenDialog>
        </div>
    );
};
