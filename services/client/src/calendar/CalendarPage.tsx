import { Button, Container, Box } from "@material-ui/core";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Subscription } from "rxjs";
import CalendarViewDay from "@material-ui/icons/CalendarViewDay";
import { map } from "rxjs/operators";
import { eventsService } from "../shared/events.service";
import { wrapSubscribe } from "../shared/utility";
import { CommunityEvent } from "../types/CommunityEvent";
import { Calendar, momentLocalizer } from "react-big-calendar";
import { Legend, buildPalette } from "./Legend";
import { useMobileCheck } from "../shared/isMobile";
import { CalendarViewEvent } from "./CalendarViewEvent";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { CalendarEvent } from "./types";
import { hubService } from "../shared/hub.service";

export function extractEvent(event: CommunityEvent): CalendarEvent {
    const start = moment(event.startDate);
    const end = moment(event.endDate);
    return {
        start: new Date(start.toDate()),
        end: new Date(end.toDate()),
        title: event.name,
        location: event.location,
        link: event.link || "",
        groupName: event.community?.name || "Other",
        color: event.community?.primaryColor,
    };
}

export interface CalendarEventState {
    events: CalendarEvent[];
    legend: { [key: string]: string };
    icalLink: string;
    gcalHref: string;
}

/**
 * View model to get a list of upcoming events from the event service
 */
export function useEventList(): CalendarEventState {
    const [state, setState] = useState<CalendarEventState>({
        events: [],
        legend: {},
        icalLink: "",
        gcalHref: "",
    });

    useEffect(() => {
        // local copy
        const events$ = eventsService.getCurrentHubEvents().pipe(
            map((events) => {
                const calEvents = events.map((el) => extractEvent(el));
                const legend = buildPalette(calEvents);
                return { events: calEvents, legend: legend };
            })
        );
        const calId$ = hubService.getCurrentHub().pipe(
            map((hub) => {
                if (hub.googleCalendarId) {
                    const icalLink = `https://calendar.google.com/calendar/ical/${hub.googleCalendarId}/public/basic.ics`;
                    const gcalHref = `https://calendar.google.com/calendar/embed?src=${hub.googleCalendarId}&ctz=America%2FEdmonton`;
                    return { icalLink: icalLink, gcalHref: gcalHref };
                } else {
                    return {};
                }
            })
        );
        const subscriptions: Subscription[] = [
            wrapSubscribe<any>(events$, (evts) => {
                setState((state) => ({ ...state, ...evts }));
            }),
            wrapSubscribe<any>(calId$, (cals) => {
                setState((state) => ({ ...state, ...cals }));
            }),
        ];

        return () => {
            subscriptions.map((it) => it.unsubscribe());
        };
    }, []);

    return state;
}

export const CalendarPage = () => {
    const calState = useEventList();
    const mobile = useMobileCheck();

    const openGCal = () => {
        window.open(calState.gcalHref);
    };

    const localizer = momentLocalizer(moment);

    const eventStyleGetter = (
        event: CalendarEvent,
        start: any,
        end: any,
        isSelected: any
    ) => {
        const bgColor = calState.legend[event.groupName] || "#007bff";
        var style = {
            backgroundColor: bgColor,
            borderRadius: "0px",
            opacity: 0.8,
            color: "black",
            border: "0px",
            display: "block",
        };
        return {
            style: style,
        };
    };

    // Buttons with links to source calendars. These only show up if a hub has a
    // google calendar associated with it
    const buttonBar = (
        <div className="mt-2">
            <Box m={2} display="flex" flexDirection="row">
                <Box p={0.5}>
                    <Button
                        variant="outlined"
                        color="primary"
                        href={calState.gcalHref}
                    >
                        <CalendarViewDay />
                        iCal Link{" "}
                    </Button>
                </Box>
                <Box p={0.5}>
                    <Button
                        onClick={openGCal}
                        variant="outlined"
                        color="secondary"
                    >
                        <CalendarViewDay /> Open in Google Calendar{" "}
                    </Button>
                </Box>
            </Box>
        </div>
    );

    return (
        <div>
            <Container>
                <h2>Upcoming Events</h2>
                <Calendar
                    localizer={localizer}
                    defaultDate={new Date()}
                    defaultView={mobile ? "agenda" : "month"}
                    views={["month", "week", "agenda"]}
                    formats={{
                        agendaTimeRangeFormat: ({ start, end }) => {
                            return (
                                moment(start).format("h:mm") +
                                "-" +
                                moment(end).format("h:mm A")
                            );
                        },
                    }}
                    events={calState.events}
                    eventPropGetter={eventStyleGetter}
                    components={{
                        month: { event: CalendarViewEvent },
                        week: { event: CalendarViewEvent },
                        // agenda: { event: AgendaEvent },
                    }}
                    style={{ height: "75vh" }}
                />
                {calState?.gcalHref ? buttonBar : null}
                <Legend palette={calState.legend}></Legend>
            </Container>
        </div>
    );
};
