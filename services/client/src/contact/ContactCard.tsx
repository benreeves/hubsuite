import React, { FC, FunctionComponent } from "react";
import cx from "clsx";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Paper } from "@material-ui/core";
import { Contact } from "../types/Contact";

const usePersonStyles = makeStyles(() => ({
    root: {
        padding: "1em",
    },
    text: {
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        overflow: "hidden",
    },
    name: {
        fontWeight: 600,
    },
    caption: {
        fontSize: "0.875rem",
        color: "#758392",
        marginTop: -4,
    },
    avatarWrapper: {
        paddingRight: "0.5em",
    },
    btn: {
        // borderRadius: 20,
        // padding: '0.125rem 0.75rem',
        // borderColor: '#becddc',
        // fontSize: '0.75rem',
    },
}));

interface ContactProps {
    name: string;
    avatar: string;
    email: string;
}

export const ContactCard = ({ name, avatar, email }: ContactProps) => {
    const styles = usePersonStyles();
    return (
        <Paper className={styles.root}>
            <Box display="flex" flexDirection="row">
                <Box className={styles.avatarWrapper}>
                    <Avatar src={avatar} />
                </Box>
                <Box display="flex" flexDirection="row" flexGrow={1}>
                    <Box flexGrow={1}>
                        <div className={cx(styles.name, styles.text)}>
                            {name}
                        </div>
                        <div className={cx(styles.caption, styles.text)}>
                            {email}
                        </div>
                    </Box>
                    <Box position={"middle"} alignContent="flex-end">
                        <Button className={styles.btn} variant={"outlined"}>
                            Contact
                        </Button>
                    </Box>
                </Box>
            </Box>
        </Paper>
    );
};
